from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow.python.platform
import tensorflow as tf
import cv2
import numpy as np
import math
import os

def read_bin_file(filename_queue):
  class CIFAR10Record(object):
    pass
  result = CIFAR10Record()

  label_bytes = 4 
  result.height = 15
  result.width = 15
  result.depth = 4
  image_bytes = result.height * result.width * result.depth

  record_bytes = label_bytes + image_bytes

  reader = tf.FixedLengthRecordReader(record_bytes=record_bytes)
  result.key, value = reader.read(filename_queue)

  record_bytes = tf.decode_raw(value, tf.uint8)

  result.label = tf.cast(tf.slice(record_bytes, [0], [label_bytes]),tf.int32)

  depth_major = tf.reshape(tf.slice(record_bytes, [label_bytes], [image_bytes]),
                           [result.depth, result.height, result.width])

  result.uint8image = tf.transpose(depth_major, [1, 2, 0])

  return result

def read_images(rgb_img, nir_img, ndvi_img, threshold):
	class CIFAR10Record(object):
		pass
	result = CIFAR10Record()

	rgb_temp_ = cv2.imread(rgb_img, cv2.IMREAD_COLOR)
	nir_temp_ = cv2.imread(nir_img, cv2.IMREAD_GRAYSCALE)
	ndvi_temp_ = cv2.imread(ndvi_img, cv2.IMREAD_GRAYSCALE)

	rgb_padded_ = np.zeros((rgb_temp_.shape[0]+14,rgb_temp_.shape[1]+14,3), np.uint8)
	nir_padded_ = np.zeros((rgb_temp_.shape[0]+14,rgb_temp_.shape[1]+14), np.uint8)	

	rgb_padded_[0+7:rgb_padded_.shape[0]-7, 0+7:rgb_padded_.shape[1]-7, :] = rgb_temp_
	nir_padded_[0+7:nir_padded_.shape[0]-7, 0+7:nir_padded_.shape[1]-7] = nir_temp_

	samples = 0
	#ndvi_samples_ = np.zeros((ndvi_temp_.shape[0], ndvi_temp_.shape[1]), np.uint8)

	for c in xrange(0, ndvi_temp_.shape[0]):
		for r in xrange(0, ndvi_temp_.shape[1]):

			if(ndvi_temp_[c,r] >= threshold):
				#ndvi_samples_[c,r] = 255
				samples = samples + 1

	patches_ = np.zeros((samples, 15,15, 4), np.uint8)
	indexes_ = np.zeros((samples, 2))
	samples = 0

	for c in xrange(0, ndvi_temp_.shape[0]):
		for r in xrange(0, ndvi_temp_.shape[1]):
			if(ndvi_temp_[c,r] >= threshold):
				patches_[samples, :, :, 0:3] = rgb_padded_[c:c+15,r:r+15,:]
				patches_[samples, :, :, 3] = nir_padded_[c:c+15,r:r+15]
				indexes_[samples,0] = c 
				indexes_[samples,1] = r 
				samples = samples +1


	#image_uint8 = tf.convert_to_tensor(patches_, dtype=tf.int32)
	#indexes = tf.convert_to_tensor(indexes_, dtype=tf.int32)

	BinaryFile = np.zeros((samples,904), dtype=np.uint8)
	count = 0

	for j in xrange(0,samples):

		#SOIL IMAGE ADJOINT
		b = patches_[j,:,:,0]
		g = patches_[j,:,:,1]
		r = patches_[j,:,:,2]
		n = patches_[j,:,:,3]
		#ndvi = tempndvi[j, :,:,:]

		g = g.reshape(1,225)
		r = r.reshape(1,225)
		b = b.reshape(1,225)
		n = n.reshape(1,225)
		#ndvi = ndvi.reshape(1,225)

		BinaryFile[count,4:904] = np.concatenate( (r,g,b,n), axis=1 )
		BinaryFile[count,0]= indexes_[count,0]%255
		BinaryFile[count,1]= math.floor(indexes_[count,0]/255)
		BinaryFile[count,2]= indexes_[count,1]%255
		BinaryFile[count,3]= math.floor(indexes_[count,1]/255)

		#print(BinaryFile[count,0:4])
		#print(indexes_[count,:])

		count = count +1

	newFile = open (os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Temp_Folder', 'batch_segmentation.bin'), "wb")
	newFileByteArray = bytearray(BinaryFile)
	newFile.write(newFileByteArray)

	return samples




def read_images_classification(rgb_img, nir_img, veg_mask_):
	class CIFAR10Record(object):
		pass
	result = CIFAR10Record()

	rgb_temp_ = cv2.imread(rgb_img, cv2.IMREAD_COLOR)
	nir_temp_ = cv2.imread(nir_img, cv2.IMREAD_GRAYSCALE)
	#veg_mask_ = cv2.imread("/home/ciro/Scrivania/Integration_Week_IAS_2016/Temp_Folder/seg_img.png", cv2.IMREAD_GRAYSCALE)

	rgb_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60,3), np.uint8)
	nir_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60), np.uint8)	

	rgb_padded_[0+30:rgb_padded_.shape[0]-30, 0+30:rgb_padded_.shape[1]-30, :] = rgb_temp_
	nir_padded_[0+30:nir_padded_.shape[0]-30, 0+30:nir_padded_.shape[1]-30] = nir_temp_

	samples = 0
	#ndvi_samples_ = np.zeros((veg_mask_.shape[0], veg_mask_.shape[1]), np.uint8)

	for c in xrange(0, veg_mask_.shape[0]):
		for r in xrange(0, veg_mask_.shape[1]):

			if(veg_mask_[c,r] >= 254):
				#ndvi_samples_[c,r] = 255
				samples = samples + 1

	patches_ = np.zeros((samples, 61,61, 4), np.uint8)
	indexes_ = np.zeros((samples, 2))
	samples = 0

	for c in xrange(0, veg_mask_.shape[0]):
		for r in xrange(0, veg_mask_.shape[1]):
			if(veg_mask_[c,r] >= 254):
				patches_[samples, :, :, 0:3] = rgb_padded_[c:c+61,r:r+61,:]
				patches_[samples, :, :, 3] = nir_padded_[c:c+61,r:r+61]
				indexes_[samples,0] = c 
				indexes_[samples,1] = r 
				samples = samples +1


	#image_uint8 = tf.convert_to_tensor(patches_, dtype=tf.int32)
	#indexes = tf.convert_to_tensor(indexes_, dtype=tf.int32)

	BinaryFile = np.zeros((samples,14888), dtype=np.uint8)
	count = 0

	for j in xrange(0,samples):

		#SOIL IMAGE ADJOINT
		b = patches_[j,:,:,0]
		g = patches_[j,:,:,1]
		r = patches_[j,:,:,2]
		n = patches_[j,:,:,3]
		#ndvi = tempndvi[j, :,:,:]

		g = g.reshape(1,3721)
		r = r.reshape(1,3721)
		b = b.reshape(1,3721)
		n = n.reshape(1,3721)
		#ndvi = ndvi.reshape(1,225)

		BinaryFile[count,4:14888] = np.concatenate( (r,g,b,n), axis=1 )
		BinaryFile[count,0]= indexes_[count,0]%255
		BinaryFile[count,1]= math.floor(indexes_[count,0]/255)
		BinaryFile[count,2]= indexes_[count,1]%255
		BinaryFile[count,3]= math.floor(indexes_[count,1]/255)

		#print(BinaryFile[count,0:4])
		#print(indexes_[count,:])

		count = count +1

	newFile = open (os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Temp_Folder', 'batch_classification.bin'), "wb")
	newFileByteArray = bytearray(BinaryFile)
	newFile.write(newFileByteArray)

	return samples


def read_images_sampled(rgb_img, nir_img, veg_mask_):
	class CIFAR10Record(object):
		pass
	result = CIFAR10Record()

	rgb_temp_ = cv2.imread(rgb_img, cv2.IMREAD_COLOR)
	nir_temp_ = cv2.imread(nir_img, cv2.IMREAD_GRAYSCALE)

	rgb_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60,3), np.uint8)
	nir_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60), np.uint8)	

	rgb_padded_[0+30:rgb_padded_.shape[0]-30, 0+30:rgb_padded_.shape[1]-30, :] = rgb_temp_
	nir_padded_[0+30:nir_padded_.shape[0]-30, 0+30:nir_padded_.shape[1]-30] = nir_temp_

	samples = 0
	samples = 25*len(veg_mask_)

	patches_ = np.zeros((samples, 61,61, 4), np.uint8)
	indexes_ = np.zeros((samples, 2))
	samples = 0

	for iter in xrange(0, len(veg_mask_)):
		for iterissimo in xrange(0, 25):
				c = veg_mask_[iter][iterissimo][0]
				r = veg_mask_[iter][iterissimo][1]
				patches_[samples, :, :, 0:3] = rgb_padded_[c:c+61,r:r+61,:]
				patches_[samples, :, :, 3] = nir_padded_[c:c+61,r:r+61]
				indexes_[samples,0] = c 
				indexes_[samples,1] = r 
				samples = samples +1


	BinaryFile = np.zeros((samples,14888), dtype=np.uint8)
	count = 0

	for j in xrange(0,samples):

		#SOIL IMAGE ADJOINT
		b = patches_[j,:,:,0]
		g = patches_[j,:,:,1]
		r = patches_[j,:,:,2]
		n = patches_[j,:,:,3]
		#ndvi = tempndvi[j, :,:,:]

		g = g.reshape(1,3721)
		r = r.reshape(1,3721)
		b = b.reshape(1,3721)
		n = n.reshape(1,3721)
		#ndvi = ndvi.reshape(1,225)

		BinaryFile[count,4:14888] = np.concatenate( (r,g,b,n), axis=1 )
		BinaryFile[count,0]= indexes_[count,0]%255
		BinaryFile[count,1]= math.floor(indexes_[count,0]/255)
		BinaryFile[count,2]= indexes_[count,1]%255
		BinaryFile[count,3]= math.floor(indexes_[count,1]/255)

		#print(BinaryFile[count,0:4])
		#print(indexes_[count,:])

		count = count +1

	newFile = open (os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Temp_Folder', 'batch_classification.bin'), "wb")
	newFileByteArray = bytearray(BinaryFile)
	newFile.write(newFileByteArray)

	return samples