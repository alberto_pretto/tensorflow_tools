from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time

import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import cv2
import time
import os
import segmentation_functions
import segmentation_input
import sys, getopt
import argparse
import tarfile
import gzip
import re

from six.moves import urllib

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('image_name', '1432227588',
                           """Path to the data directory.""")

eval_dir = os.path.dirname(os.path.realpath(__file__))
eval_data = 'test'
checkpoint_dir_seg = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Training_Checkpoints_Segmentation')
checkpoint_dir_class = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Training_Checkpoints_Classification')
eval_interval_secs = 60*5
num_examples = 10000
run_one = False

DATA_URL = 'http://googledrive.com/host/0B9NPzvmeCK3fZHk4WWJibVdIWjQ'


seg_img_ = np.zeros((464,622,1))
detected_blobs_ = np.zeros((464,622,1))
class_img_ = np.zeros((464,622,3))
final_img_ = np.zeros((464,622,3))
high_img_ = np.zeros((464,622,3))

def eval_class(saver, labels, logits, images, samples):

  with tf.Session() as sess:
    ckpt = tf.train.get_checkpoint_state(checkpoint_dir_class)
    if ckpt and ckpt.model_checkpoint_path:
      path = os.path.join(os.path.dirname(os.path.realpath(__file__)), ckpt.model_checkpoint_path)
      saver.restore(sess, path)
      global_step = path.split('/')[-1].split('-')[-1]
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True,
                                         start=True))

      count = 0

      while(count < round(samples/segmentation_functions.batch_size)):

        temp = sess.run([ logits, labels])
          
        length = temp[0].shape[0]

        for iter in xrange(0, length):

          temp_logits = temp[0][iter]
          temp_ind = temp[1][iter]

          r_in = temp_ind[2] + 255*temp_ind[3]
          c_in = temp_ind[0] + 255*temp_ind[1]

          if(temp_logits[1] > temp_logits[0] and temp_logits[1] > temp_logits[2]):
            #cv2.circle(class_img_, (r_in,c_in), 2, (255,0,0))
            class_img_[c_in,r_in,0] = 255  
          #elif(temp_logits[0] > temp_logits[1] and temp_logits[0] > temp_logits[2]):
            #cv2.circle(class_img_, (r_in,c_in), 2, (0,255,0))
            #class_img_[c_in,r_in,1] = 255  
          elif(temp_logits[2] > temp_logits[1] and temp_logits[2] > temp_logits[0]):
            #cv2.circle(class_img_, (r_in,c_in), 2, (0,0,255))
            class_img_[c_in,r_in,2] = 255  

        count = count+1      

      cv2.imwrite("%s%s" % (segmentation_functions.temp_dir, '/class_img.png'), class_img_)

    except Exception as e:  
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=0) #10



def eval_segment(saver, labels, logits, images, samples):
  with tf.Session() as sess:
    ckpt = tf.train.get_checkpoint_state(checkpoint_dir_seg)
    if ckpt and ckpt.model_checkpoint_path:
      path = os.path.join(os.path.dirname(os.path.realpath(__file__)), ckpt.model_checkpoint_path)
      saver.restore(sess, path)
      global_step = path.split('/')[-1].split('-')[-1]
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True,
                                         start=True))

      count = 0 

      while(count < round(samples/segmentation_functions.batch_size)):

      	#print(os.path.dirname(os.path.realpath(__file__)))
        #print(count)

        temp = sess.run([ logits, labels])
          
        length = temp[0].shape[0]

        for iter in xrange(0, length):

          temp_logits = temp[0][iter]
          temp_ind = temp[1][iter]

          r_in = temp_ind[2] + 255*temp_ind[3]
          c_in = temp_ind[0] + 255*temp_ind[1]

          if(temp_logits[1] > temp_logits[0]):
            seg_img_[c_in,r_in,0] = 255  
          
        count = count+1  
         
      cv2.imwrite("%s%s" % (segmentation_functions.temp_dir, '/seg_img.png'), seg_img_)

    except Exception as e:  
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=0)



def segmentation(samples):

  with tf.Graph().as_default():

    images, labels = segmentation_functions.inputs(eval_data='test')

    logits = segmentation_functions.inference(images)

    variable_averages = tf.train.ExponentialMovingAverage(
        segmentation_functions.MOVING_AVERAGE_DECAY)
    variables_to_restore = {}
    for v in tf.all_variables():     
      if v in tf.trainable_variables():
        restore_name = variable_averages.average_name(v)
      else:
        restore_name = v.op.name
      variables_to_restore[restore_name] = v
    saver = tf.train.Saver(variables_to_restore)

    graph_def = tf.get_default_graph().as_graph_def()

    eval_segment(saver, labels, logits, images, samples)

def classification(samples):

  with tf.Graph().as_default():

    images, labels = segmentation_functions.inputs_class(eval_data='test')

    logits = segmentation_functions.inference_class(images)

    variable_averages = tf.train.ExponentialMovingAverage(
        segmentation_functions.MOVING_AVERAGE_DECAY)
    variables_to_restore = {}
    for v in tf.all_variables():     
      if v in tf.trainable_variables():
        restore_name = variable_averages.average_name(v)
      else:
        restore_name = v.op.name
      variables_to_restore[restore_name] = v
    saver = tf.train.Saver(variables_to_restore)

    graph_def = tf.get_default_graph().as_graph_def()

    eval_class(saver, labels, logits, images, samples)


def FindBlobs():

  blobs = []

  for c in xrange(seg_img_.shape[0]):
    for r in xrange(seg_img_.shape[1]):

      temp_blob = []
      temp = []

      if( seg_img_[c,r] >= 200 and detected_blobs_[c,r] == 0 ):
        temp_blob.append([c,r])
        temp.append([c,r])
        detected_blobs_[c,r] = 255

        while(len(temp) != 0):
          temp_point = temp[0]
          for cc in xrange(-1,2):
            for rr in xrange(-1,2):
              if(cc==0 and rr==0):
                continue
              if(seg_img_[temp_point[0]+cc,temp_point[1]+rr] >= 200 and detected_blobs_[temp_point[0]+cc,temp_point[1]+rr] == 0):
                temp_blob.append([temp_point[0]+cc,temp_point[1]+rr])
                temp.append([temp_point[0]+cc,temp_point[1]+rr])
                detected_blobs_[temp_point[0]+cc,temp_point[1]+rr] = 255
          temp.remove(temp[0])
        blobs.append(temp_blob)

  iter = 0

  while(iter < len(blobs)):
    if(len(blobs[iter])<25):
      for iterirerrimo in xrange(0, len(blobs[iter])):
        detected_blobs_[blobs[iter][iterirerrimo][0],blobs[iter][iterirerrimo][1]] = 0
      blobs.pop(iter)
    else:
      iter = iter + 1  

  cv2.imwrite("%s%s" % (segmentation_functions.temp_dir, '/seg_without_small_blobs_img.png'), detected_blobs_)

  #cv2.imshow("final result", seg_img_)
  #cv2.imshow("detected blobs", detected_blobs_)
  #cv2.waitKey(0)

  return blobs
        
def BlobsSampling(blobs):

  sampled_blobs_ = []

  for iter in xrange(0, len(blobs)):

    random_sampling = np.random.choice(len(blobs[iter]), 25, replace=False)
    temp = []
  
    for iterissimo in xrange(0, 25):
      temp.append(blobs[iter][random_sampling[iterissimo]])

    sampled_blobs_.append(temp)
  
  return sampled_blobs_

def OutputImages(rgb_img, sampled_mask_, det_blobs_):

  for iter in xrange(0, len(sampled_mask_)):
    weed = 0
    plant = 0
    for iterissimo in xrange(0, 25):
      if(class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[0]] >= 200 and 
        class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[2]] <=10 ):

        weed = weed + 1
      elif(class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[0]] <=10 and 
        class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[2]] >=200 ):

        plant = plant + 1  

    for iterissimo in xrange(0, len(det_blobs_[iter])):
      if(weed > plant):
        final_img_[det_blobs_[iter][iterissimo][0], det_blobs_[iter][iterissimo][1], 0] = 255
      elif(plant >= weed):
        final_img_[det_blobs_[iter][iterissimo][0], det_blobs_[iter][iterissimo][1], 2] = 255   

  cv2.imwrite("%s%s" % (segmentation_functions.temp_dir, '/final_img.png'), final_img_) 
 
  rgb=cv2.imread(rgb_img, cv2.IMREAD_COLOR)

  cv2.imwrite("%s%s" % (segmentation_functions.temp_dir, '/marked_img.png'), 0.5*rgb+final_img_*0.4) 


def maybe_download_and_extract():
  """Download and extract the sNet, cNet and example images from Ciro Potena's web storage."""
  dest_directory = segmentation_functions.temp_dir 
  filename = 'TrainingData.tar.gz'
  filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)

  
  if not os.path.exists(dest_directory):
    print('Downloading %s' % (filename))
    filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath)

    print()
    statinfo = os.stat(filepath)
    print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
    tarfile.open(filepath, 'r:gz').extractall(os.path.dirname(os.path.realpath(__file__)))


def main(argv): 

  tensorflow.python.platform.logging.set_verbosity(3)
  maybe_download_and_extract()

  rgb_img = "%s%s"%(FLAGS.image_name,'_rgb.png')
  nir_img = "%s%s"%(FLAGS.image_name,'_nir.png')
  ndvi_img = "%s%s"%(FLAGS.image_name,'_ndvi.png')

  samples = segmentation_input.read_images(rgb_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Segmentation_Images_NoBin', rgb_img)),
  	                             		   nir_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),  'Segmentation_Images_NoBin',nir_img)),
  	                             		   ndvi_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),  'Segmentation_Images_NoBin',ndvi_img)),
  	                             		   threshold=150)

  seg_time = 0
  class_time = 0
  
  if(1):
    start_time = time.time()  
    segmentation(samples)
    seg_time = time.time() - start_time

  
  detected_blobs_ = FindBlobs()

  sampled_blobs_ = BlobsSampling(blobs=detected_blobs_)

  
  samples_class = segmentation_input.read_images_sampled(rgb_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Segmentation_Images_NoBin', rgb_img)),
                                       nir_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Segmentation_Images_NoBin', nir_img)), veg_mask_=sampled_blobs_)  
  if(1):
    start_time = time.time()  
    classification(samples_class)
    class_time = time.time() - start_time

  OutputImages(rgb_img = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),  'Segmentation_Images_NoBin', rgb_img)), sampled_mask_=sampled_blobs_, det_blobs_=detected_blobs_)

  print("%s %f" % ('Time elapsed during the vegetation detection phase: ', seg_time))
  print("%s %f" % ('Time elapsed during the vegetation classification phase: ', class_time))
  

if __name__ == '__main__':
  tf.app.run()
