from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re
import sys

import tensorflow.python.platform
from six.moves import urllib
from six.moves import xrange  
import tensorflow as tf

import segmentation_input
import classification_input
from tensorflow.python.platform import gfile


batch_size = 128
data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Segmentation_Images')
temp_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Temp_Folder')


IMAGE_SIZE = 15

NUM_CLASSES = 2
NUM_EXAMPLES_PER_EPOCH_FOR_TRAIN = 500
NUM_EXAMPLES_PER_EPOCH_FOR_EVAL = 200

MOVING_AVERAGE_DECAY = 0.9999     # The decay to use for the moving average.
NUM_EPOCHS_PER_DECAY = 350.0      # Epochs after which learning rate decays.
LEARNING_RATE_DECAY_FACTOR = 0.1  # Learning rate decay factor.
INITIAL_LEARNING_RATE = 0.1       # Initial learning rate.

TOWER_NAME = 'tower'

def _variable_on_cpu(name, shape, initializer):
  with tf.device('/cpu:0'):
    var = tf.get_variable(name, shape, initializer=initializer)
  return var


def _variable_with_weight_decay(name, shape, stddev, wd):
  var = _variable_on_cpu(name, shape,
                         tf.truncated_normal_initializer(stddev=stddev))
  if wd:
    weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
    tf.add_to_collection('losses', weight_decay)
  return var



def _generate_image_and_label_batch(image, label, min_queue_examples):
  num_preprocess_threads = 16
  images, label_batch = tf.train.batch(
      [image, label],
      batch_size=batch_size,
      num_threads=num_preprocess_threads,
      capacity=batch_size*6)

  return images, label_batch

def inputs_class(eval_data):
  filenames = [os.path.join(temp_dir, 'batch_classification.bin')]

  num_examples_per_epoch = NUM_EXAMPLES_PER_EPOCH_FOR_EVAL

  # Create a queue that produces the filenames to read.
  filename_queue = tf.train.string_input_producer(filenames)

  # Read examples from files in the filename queue.
  read_input = classification_input.read_bin_file(filename_queue)

  reshaped_image = tf.cast(read_input.uint8image, tf.float32)

  # Subtract off the mean and divide by the variance of the pixels.
  float_image = tf.image.per_image_whitening(reshaped_image)

  # Ensure that the random shuffling has good mixing properties.
  min_fraction_of_examples_in_queue = 0.4
  min_queue_examples = int(num_examples_per_epoch *
                           min_fraction_of_examples_in_queue)

  # Generate a batch of images and labels by building up a queue of examples.
  return _generate_image_and_label_batch(float_image, read_input.label,
                                         min_queue_examples)



def inputs(eval_data):
  filenames = [os.path.join(temp_dir, 'batch_segmentation.bin')]

  num_examples_per_epoch = NUM_EXAMPLES_PER_EPOCH_FOR_EVAL

  filename_queue = tf.train.string_input_producer(filenames)

  read_input = segmentation_input.read_bin_file(filename_queue)

  reshaped_image = tf.cast(read_input.uint8image, tf.float32)

  float_image = tf.image.per_image_whitening(reshaped_image)

  min_fraction_of_examples_in_queue = 0.4
  min_queue_examples = int(num_examples_per_epoch *
                           min_fraction_of_examples_in_queue)

  return _generate_image_and_label_batch(float_image, read_input.label, min_queue_examples)


def inference(images):
  # conv1
  with tf.variable_scope('conv_seg') as scope:
    kernel = _variable_with_weight_decay('weights_seg', shape=[3, 3, 4, 5],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(images, kernel, [1, 3, 3, 1], padding='SAME')
    biases = _variable_on_cpu('biases_seg', [5], tf.constant_initializer(0.0))
    bias = tf.nn.bias_add(conv, biases)
    conv1 = tf.nn.relu(bias, name=scope.name)

  # norm1
  norm1 = tf.nn.lrn(conv1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm1_seg')

  # local3
  with tf.variable_scope('local_seg') as scope:
    dim = 1
    for d in norm1.get_shape()[1:].as_list():
      dim *= d
    reshape = tf.reshape(norm1, [batch_size, dim])

    weights = _variable_with_weight_decay('weights_seg_loc', shape=[dim, 50],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases_seg_loc', [50 ], tf.constant_initializer(0.1))
    local3 = tf.nn.relu_layer(reshape, weights, biases, name=scope.name)

  with tf.variable_scope('softmax_linear_seg_loc') as scope:
    weights = _variable_with_weight_decay('weights_soft_seg', [50 , NUM_CLASSES],
                                          stddev=1/(50 ), wd=0.0)
    biases = _variable_on_cpu('biases_soft_seg', [NUM_CLASSES],
                              tf.constant_initializer(0.0))
    softmax_linear = tf.nn.xw_plus_b(local3, weights, biases, name=scope.name)

  return softmax_linear

def inference_class(images):


  # conv1
  with tf.variable_scope('conv1') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 4, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(images, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.0))
    bias = tf.nn.bias_add(conv, biases)
    conv1 = tf.nn.relu(bias, name=scope.name)

  # norm1
  norm1 = tf.nn.lrn(conv1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm1')

  # conv2
  with tf.variable_scope('conv2') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 64, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(norm1, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.1))
    bias = tf.nn.bias_add(conv, biases)
    conv2 = tf.nn.relu(bias, name=scope.name)

  # norm2
  norm2 = tf.nn.lrn(conv2, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm2')


  # local3
  with tf.variable_scope('local3') as scope:
    # Move everything into depth so we can perform a single matrix multiply.
    dim = 1
    for d in norm2.get_shape()[1:].as_list():
      dim *= d
    reshape = tf.reshape(norm2, [batch_size, dim])

    weights = _variable_with_weight_decay('weights', shape=[dim, 384],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [384], tf.constant_initializer(0.1))

    descriptors = tf.matmul(reshape, weights)

    local3 = tf.nn.relu_layer(reshape, weights, biases, name=scope.name)

  # local4
  with tf.variable_scope('local4') as scope:
    weights = _variable_with_weight_decay('weights', shape=[384, 192],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [192], tf.constant_initializer(0.1))
    local4 = tf.nn.relu_layer(local3, weights, biases, name=scope.name)

  # softmax, i.e. softmax(WX + b)
  with tf.variable_scope('softmax_linear') as scope:
    weights = _variable_with_weight_decay('weights', [192, 3],
                                          stddev=1/192.0, wd=0.0)
    biases = _variable_on_cpu('biases', [3],
                              tf.constant_initializer(0.0))
    softmax_linear = tf.nn.xw_plus_b(local4, weights, biases, name=scope.name)

  return softmax_linear
