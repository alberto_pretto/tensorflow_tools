#!/usr/bin/env python

from __future__ import print_function

import roslib 
import sys
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math   

def prova():
  bridge = CvBridge()
  pub_rgb = rospy.Publisher('/stereo_rgb/right/image_raw_sync', Image)
  pub_nir = rospy.Publisher('/stereo_nir/right/image_raw_sync', Image)
  rospy.init_node('im_pub', anonymous=True)

  file_nir = open("nir.txt","rw+")
  #file_nir = open("nir_file.txt","rw+")
  #rgb_name = file_rgb.readline();
  nir_name = file_nir.readline();
  rate = rospy.Rate(0.3)
  while(nir_name):
    rgb_img = cv2.imread(("%s%s")%('/home/flourish/Desktop/rgb/',nir_name[2:len(nir_name)-1]), cv2.IMREAD_COLOR)
    nir_img = cv2.imread(("%s%s")%('/home/flourish/Desktop/nir/',nir_name[2:len(nir_name)-1]), cv2.IMREAD_GRAYSCALE)
    pub_nir.publish(bridge.cv2_to_imgmsg(nir_img, "mono8"))
    pub_rgb.publish(bridge.cv2_to_imgmsg(rgb_img, "bgr8"))
    
    #rgb_name = file_rgb.readline();
    nir_name = file_nir.readline();
    rate.sleep()


if __name__ == '__main__':
    prova()
