#!/usr/bin/env python

from __future__ import print_function

import roslib 
import sys
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math


eval_data = 'test'
checkpoint_dir_seg = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Training_Checkpoints_Segmentation')
checkpoint_dir_class = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Training_Checkpoints_Classification')

def inference_class(images):


  # conv1
  with tf.variable_scope('conv1') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 4, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(images, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.0))
    bias = tf.nn.bias_add(conv, biases)
    conv1 = tf.nn.relu(bias, name=scope.name)

  # norm1
  norm1 = tf.nn.lrn(conv1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm1')

  # conv2
  with tf.variable_scope('conv2') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 64, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(norm1, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.1))
    bias = tf.nn.bias_add(conv, biases)
    conv2 = tf.nn.relu(bias, name=scope.name)

  # norm2
  norm2 = tf.nn.lrn(conv2, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm2')


  # local3
  with tf.variable_scope('local3') as scope:
    # Move everything into depth so we can perform a single matrix multiply.
    dim = 1
    for d in norm2.get_shape()[1:].as_list():
      dim *= d
    reshape = tf.reshape(norm2, [20, dim])

    weights = _variable_with_weight_decay('weights', shape=[dim, 384],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [384], tf.constant_initializer(0.1))

    descriptors = tf.matmul(reshape, weights)

    local3 = tf.nn.relu_layer(reshape, weights, biases, name=scope.name)

  # local4
  with tf.variable_scope('local4') as scope:
    weights = _variable_with_weight_decay('weights', shape=[384, 192],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [192], tf.constant_initializer(0.1))
    local4 = tf.nn.relu_layer(local3, weights, biases, name=scope.name)

  # softmax, i.e. softmax(WX + b)
  with tf.variable_scope('softmax_linear') as scope:
    weights = _variable_with_weight_decay('weights', [192, 3],
                                          stddev=1/192.0, wd=0.0)
    biases = _variable_on_cpu('biases', [3],
                              tf.constant_initializer(0.0))
    softmax_linear = tf.nn.xw_plus_b(local4, weights, biases, name=scope.name)

  return softmax_linear

def image_whitening(rgb_inp, nir_inp):

  temp = np.zeros((rgb_inp.shape[0], rgb_inp.shape[1], 4), np.float32)
  temp[:,:,0:3] = rgb_inp
  temp[:,:,3] = nir_inp

  mean = 0

  for c in xrange(0, rgb_inp.shape[0]):
    for r in xrange(0, rgb_inp.shape[1]):
      mean = mean + temp[c,r,0] + temp[c,r,1] + temp[c,r,2] + temp[c,r,3]

  mean = mean/(rgb_inp.shape[0]*rgb_inp.shape[1]*4)

  stddv = 0

  for c in xrange(0, rgb_inp.shape[0]):
    for r in xrange(0, rgb_inp.shape[1]):
      stddv = stddv + pow((temp[c,r,0] - mean),2) + pow((temp[c,r,1] - mean),2) + pow((temp[c,r,2] - mean),2) + pow((temp[c,r,3] - mean),2) 

  stddv = pow( stddv/(rgb_inp.shape[0]*rgb_inp.shape[1]*4) ,0.5)
  if(1/(pow(rgb_inp.shape[0]*rgb_inp.shape[1]*4,0.5)) > stddv):
    stddv = 1/(pow(rgb_inp.shape[0]*rgb_inp.shape[1]*4,0.5))

  temp = (temp-mean)/stddv

  return temp[:,:,0:3], temp[:,:,3]

def _variable_on_cpu(name, shape, initializer):
  with tf.device('/cpu:0'):
    var = tf.get_variable(name, shape, initializer=initializer)
  return var


def _variable_with_weight_decay(name, shape, stddev, wd):
  var = _variable_on_cpu(name, shape,
                         tf.truncated_normal_initializer(stddev=stddev))
  if wd:
    weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
    tf.add_to_collection('losses', weight_decay)
  return var


def read_images_sampled(rgb_img, nir_img, veg_mask_):
  class CIFAR10Record(object):
    pass
  result = CIFAR10Record()

  rgb_temp_ = rgb_img
  nir_temp_ = nir_img

  rgb_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60,3), np.float32)
  nir_padded_ = np.zeros((rgb_temp_.shape[0]+60,rgb_temp_.shape[1]+60), np.float32) 

  rgb_padded_[0+30:rgb_padded_.shape[0]-30, 0+30:rgb_padded_.shape[1]-30, :] = rgb_temp_
  nir_padded_[0+30:nir_padded_.shape[0]-30, 0+30:nir_padded_.shape[1]-30] = nir_temp_

  samples = 0
  samples = 25*len(veg_mask_)

  patches_ = np.zeros((samples, 61,61, 4), np.float32)
  indexes_ = np.zeros((samples, 2), np.int32)
  samples = 0

  for iter in xrange(0, len(veg_mask_)):
    for iterissimo in xrange(0, 20):
        c = veg_mask_[iter][iterissimo][0]
        r = veg_mask_[iter][iterissimo][1]
        patches_[samples, :, :, 0:3] = rgb_padded_[c:c+61,r:r+61,:]
        patches_[samples, :, :, 3] = nir_padded_[c:c+61,r:r+61]
        indexes_[samples,0] = c 
        indexes_[samples,1] = r 
        samples = samples +1

  return samples, patches_, indexes_


def inputs(eval_data):

  reshaped_image = tf.cast(eval_data, tf.float32)

  return reshaped_image

def FindBlobs(seg_img_):

  blobs = []
  detected_blobs_ = np.zeros((seg_img_.shape[0],seg_img_.shape[1],1))
  for c in xrange(seg_img_.shape[0]):
    for r in xrange(seg_img_.shape[1]):

      temp_blob = []
      temp = []

      if( seg_img_[c,r] >= 200 and detected_blobs_[c,r] == 0 ):
        temp_blob.append([c,r])
        temp.append([c,r])
        detected_blobs_[c,r] = 255

        while(len(temp) != 0):
          temp_point = temp[0]
          for cc in xrange(-1,2):
            for rr in xrange(-1,2):
              if(cc==0 and rr==0):
                continue
              if(temp_point[0]+cc <0 or temp_point[0]+cc >= seg_img_.shape[0] or temp_point[1]+rr < 0 or temp_point[1]+rr >= seg_img_.shape[1]):
                continue
              if(seg_img_[temp_point[0]+cc,temp_point[1]+rr] >= 200 and detected_blobs_[temp_point[0]+cc,temp_point[1]+rr] == 0):
                temp_blob.append([temp_point[0]+cc,temp_point[1]+rr])
                temp.append([temp_point[0]+cc,temp_point[1]+rr])
                detected_blobs_[temp_point[0]+cc,temp_point[1]+rr] = 255
          temp.remove(temp[0])
        blobs.append(temp_blob)

  iter = 0

  while(iter < len(blobs)):
    if(len(blobs[iter])<20):
      for iterirerrimo in xrange(0, len(blobs[iter])):
        detected_blobs_[blobs[iter][iterirerrimo][0],blobs[iter][iterirerrimo][1]] = 0
      blobs.pop(iter)
    else:
      iter = iter + 1  

  return blobs
        
def BlobsSampling(blobs):

  sampled_blobs_ = []

  for iter in xrange(0, len(blobs)):

    random_sampling = np.random.choice(len(blobs[iter]), 20, replace=False)
    temp = []
  
    for iterissimo in xrange(0, 20):
      temp.append(blobs[iter][random_sampling[iterissimo]])

    sampled_blobs_.append(temp)
  
  return sampled_blobs_


def OutputImages(rgb_img, sampled_mask_, det_blobs_, class_img_):
  final_img_ = np.zeros((rgb_img.shape[0],rgb_img.shape[1],3), np.uint8)
  for iter in xrange(0, len(sampled_mask_)):
    weed = 0
    plant = 0
    for iterissimo in xrange(0, 20):
      if(class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[0]] >= 200 and 
        class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[2]] <=20 ):

        weed = weed + 1
      elif(class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[0]] <=20 and 
        class_img_[sampled_mask_[iter][iterissimo][0],sampled_mask_[iter][iterissimo][1],[2]] >=200 ):

        plant = plant + 1  

    for iterissimo in xrange(0, len(det_blobs_[iter])):
      if(weed > plant):
        final_img_[det_blobs_[iter][iterissimo][0], det_blobs_[iter][iterissimo][1], 0] = 255
      elif(plant >= weed):
        final_img_[det_blobs_[iter][iterissimo][0], det_blobs_[iter][iterissimo][1], 2] = 255   
 


  output_img = (0.5*rgb_img+final_img_*0.4)
  cv2.normalize(output_img,output_img,0,255,cv2.NORM_MINMAX)

  output_img = output_img.astype(np.uint8)
  output_img2 = cv2.resize(output_img, None, fx=2, fy=2, interpolation = cv2.INTER_LINEAR)
  """
  cv2.imshow("rgb_img",rgb_img)
  cv2.imshow("final_img_",final_img_)
  cv2.imshow("output_img_tmp",output_img)
  cv2.waitKey(50)
  """
  return output_img2


class image_converter:

  def __init__(self):
     #self.pub_class = rospy.Publisher('classification_image', Image)
     self.bridge = CvBridge()
     self.rgb_sub = message_filters.Subscriber("rgb_image", Image)
     self.nir_sub = message_filters.Subscriber("nir_image", Image)
     self.seg_sub = message_filters.Subscriber("segmented_image", Image)

     ts = message_filters.TimeSynchronizer([self.rgb_sub, self.nir_sub, self.seg_sub], 10)
     ts.registerCallback(self.callback)


  def callback(self, rgb, nir, seg):

    global seg_graph
    global sess
    global indexes_patches
    global logits
    global patches
    global labels

    cv_image_rgb = self.bridge.imgmsg_to_cv2(rgb, "bgr8")
    cv_image_nir = self.bridge.imgmsg_to_cv2(nir, "mono8")
    cv_image_seg = self.bridge.imgmsg_to_cv2(seg, "mono8")

    cv_image_rgb_res = cv2.resize(cv_image_rgb, None, fx=1, fy=1, interpolation = cv2.INTER_CUBIC)
    cv_image_nir_res = cv2.resize(cv_image_nir, None, fx=1, fy=1, interpolation = cv2.INTER_CUBIC)

    temp_rgb = cv_image_rgb_res.copy()

    """
    cv2.imshow("ndvi", cv_image_rgb_res)
    cv2.imshow("...",cv_image_nir_res)
    cv2.imshow("segmented image", cv_image_seg)
    cv2.waitKey(50)
    """

    cv_image_rgb_res = cv2.cvtColor(cv_image_rgb_res, cv2.COLOR_BGR2RGB)


    #cv2.imshow("ndvi", temp)
    #cv2.imshow("ndvi2", cv_image_rgb_res)
    #cv2.waitKey(100)
    cv_image_rgb_res_whit, cv_image_nir_res_whit = image_whitening(cv_image_rgb_res, cv_image_nir_res)        
    
    detected_blobs_ = FindBlobs(cv_image_seg)
    blobs_ = BlobsSampling(blobs=detected_blobs_)
    samples_class, patches_, indexes_ = read_images_sampled(rgb_img = cv_image_rgb_res_whit, nir_img = cv_image_nir_res_whit, veg_mask_=blobs_)    



    #cv2.imshow("rgb", cv_image_rgb_res)
    #cv2.imshow("nir", cv_image_nir_res)
    #cv2.imshow("seg", cv_image_seg)
    #cv2.waitKey(100)

    
    class_img_ = np.zeros((464,622,3), np.uint8)

    print("%s %d" % ('NUMBER OF PATCH: ', samples_class))

    with class_graph.as_default():
      with sess.as_default():
        start_time = time.time()
        iter = 0
        
        while(iter < samples_class):
          #print(iter)
          temp = sess.run([logits, indexes_patches], feed_dict={patches: patches_[iter:(iter+20)], labels: indexes_[iter:(iter+20)]})
          iter = iter+20

          ### WRITING ON OUTPUT IMAGE ###
          for iterissimo in xrange(0, temp[0].shape[0]):

            temp_logits = temp[0][iterissimo]
            temp_ind = temp[1][iterissimo]

            #print(temp_logits)

            if(temp_logits[1] > temp_logits[0] and temp_logits[1] > temp_logits[2]):
              class_img_[temp_ind[0],temp_ind[1],0] = 255  
            elif(temp_logits[0] > temp_logits[1] and temp_logits[0] > temp_logits[2]):
              class_img_[temp_ind[0],temp_ind[1],1] = 255   
            elif(temp_logits[2] > temp_logits[1] and temp_logits[2] > temp_logits[0]):
              class_img_[temp_ind[0],temp_ind[1],2] = 255  


        
        print(time.time()-start_time)
        output = OutputImages(rgb_img = temp_rgb, sampled_mask_=blobs_, det_blobs_=detected_blobs_, class_img_=class_img_)
        
        #cv2.imshow("class_img", class_img_)
        #cv2.imshow("...",cv_image_rgb_res)
        #cv2.imshow("....", cv_image_nir_res)
        #cv2.imshow("segmented", cv_image_seg)
        #cv2.imshow("segmented image", output)
        cv2.imshow("output image", output)
        cv2.waitKey(50)
        
        
    



       

def prova():
  
  global class_graph
  global sess
  global indexes_patches
  global logits
  global patches
  global labels

  ic = image_converter()
  rospy.init_node('prova', anonymous=True)

  
  class_graph=tf.Graph()
  class_graph.as_default()

  patches = tf.placeholder( tf.float32, [None, 61, 61, 4])
  labels = tf.placeholder( tf.int32, [None, 2])

  indexes_patches = tf.cast(labels, tf.int16)

  images = inputs(patches)
  logits = inference_class(images)

  variable_averages = tf.train.ExponentialMovingAverage(0.9999)
  variables_to_restore = {}
  for v in tf.all_variables():     
    if v in tf.trainable_variables():
      restore_name = variable_averages.average_name(v)
    else:
      restore_name = v.op.name
    variables_to_restore[restore_name] = v
  saver = tf.train.Saver(variables_to_restore)

  graph_def = tf.get_default_graph().as_graph_def()

  #with tf.Session() as sess:
  sess=tf.Session()
  ckpt = tf.train.get_checkpoint_state(checkpoint_dir_class)
  if ckpt and ckpt.model_checkpoint_path:
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)), ckpt.model_checkpoint_path)
    saver.restore(sess, path)
    global_step = path.split('/')[-1].split('-')[-1]
  else:
    print('No checkpoint file found')
    return
  
  rospy.spin()


if __name__ == '__main__':
    prova()
