#!/usr/bin/env python

from __future__ import print_function

import roslib 
import sys
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math


eval_data = 'test'
checkpoint_dir_seg = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Training_Checkpoints_Segmentation')

def image_whitening(rgb_inp, nir_inp):

  temp = np.zeros((rgb_inp.shape[0], rgb_inp.shape[1], 4), np.float32)
  temp[:,:,0:3] = rgb_inp
  temp[:,:,3] = nir_inp

  mean = 0

  for c in xrange(0, rgb_inp.shape[0]):
    for r in xrange(0, rgb_inp.shape[1]):
      mean = mean + temp[c,r,0] + temp[c,r,1] + temp[c,r,2] + temp[c,r,3]

  mean = mean/(rgb_inp.shape[0]*rgb_inp.shape[1]*4)

  stddv = 0

  for c in xrange(0, rgb_inp.shape[0]):
    for r in xrange(0, rgb_inp.shape[1]):
      stddv = stddv + pow((temp[c,r,0] - mean),2) + pow((temp[c,r,1] - mean),2) + pow((temp[c,r,2] - mean),2) + pow((temp[c,r,3] - mean),2) 

  stddv = pow( stddv/(rgb_inp.shape[0]*rgb_inp.shape[1]*4) ,0.5)
  if(1/(pow(rgb_inp.shape[0]*rgb_inp.shape[1]*4,0.5)) > stddv):
    stddv = 1/(pow(rgb_inp.shape[0]*rgb_inp.shape[1]*4,0.5))

  temp = (temp-mean)/stddv

  return temp[:,:,0:3], temp[:,:,3]

def _variable_on_cpu(name, shape, initializer):
  with tf.device('/cpu:0'):
    var = tf.get_variable(name, shape, initializer=initializer)
  return var


def _variable_with_weight_decay(name, shape, stddev, wd):
  var = _variable_on_cpu(name, shape,
                         tf.truncated_normal_initializer(stddev=stddev))
  if wd:
    weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
    tf.add_to_collection('losses', weight_decay)
  return var


def inference(images):
  # conv1
  with tf.variable_scope('conv_seg') as scope:
    kernel = _variable_with_weight_decay('weights_seg', shape=[3, 3, 4, 5],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(images, kernel, [1, 3, 3, 1], padding='SAME')
    biases = _variable_on_cpu('biases_seg', [5], tf.constant_initializer(0.0))
    bias = tf.nn.bias_add(conv, biases)
    conv1 = tf.nn.relu(bias, name=scope.name)

  # norm1
  norm1 = tf.nn.lrn(conv1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm1_seg')

  # local3
  with tf.variable_scope('local_seg') as scope:
    dim = 1
    for d in norm1.get_shape()[1:].as_list():
      dim *= d

    reshape = tf.reshape(norm1, [1000, dim])

    weights = _variable_with_weight_decay('weights_seg_loc', shape=[dim, 50],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases_seg_loc', [50 ], tf.constant_initializer(0.1))
    local3 = tf.nn.relu_layer(reshape, weights, biases, name=scope.name)

  with tf.variable_scope('softmax_linear_seg_loc') as scope:
    weights = _variable_with_weight_decay('weights_soft_seg', [50 , 2],
                                          stddev=1/(50 ), wd=0.0)
    biases = _variable_on_cpu('biases_soft_seg', [2],
                              tf.constant_initializer(0.0))
    softmax_linear = tf.nn.xw_plus_b(local3, weights, biases, name=scope.name)

  return softmax_linear

def read_images(rgb_img_, nir_img_, ndvi_img_, threshold):
  class CIFAR10Record(object):
    pass
  result = CIFAR10Record()

  rgb_temp_ = rgb_img_
  nir_temp_ = nir_img_
  ndvi_temp_ = ndvi_img_

  rgb_padded_ = np.zeros((rgb_temp_.shape[0]+14,rgb_temp_.shape[1]+14,3), np.float32)
  nir_padded_ = np.zeros((rgb_temp_.shape[0]+14,rgb_temp_.shape[1]+14), np.float32) 

  rgb_padded_[0+7:rgb_padded_.shape[0]-7, 0+7:rgb_padded_.shape[1]-7, :] = rgb_temp_
  nir_padded_[0+7:nir_padded_.shape[0]-7, 0+7:nir_padded_.shape[1]-7] = nir_temp_

  samples = 0
  #ndvi_samples_ = np.zeros((ndvi_temp_.shape[0], ndvi_temp_.shape[1]), np.uint8)

  for c in xrange(0, ndvi_temp_.shape[0]):
    for r in xrange(0, ndvi_temp_.shape[1]):

      if(ndvi_temp_[c,r] >= threshold):
        #ndvi_samples_[c,r] = 255
        samples = samples + 1

  patches_ = np.zeros((samples, 15,15, 4), np.float32)
  indexes_ = np.zeros((samples, 2), np.int32)
  samples = 0

  for c in xrange(0, ndvi_temp_.shape[0]):
    for r in xrange(0, ndvi_temp_.shape[1]):
      if(ndvi_temp_[c,r] >= threshold):
        patches_[samples, :, :, 0:3] = rgb_padded_[c:c+15,r:r+15,:]
        patches_[samples, :, :, 3] = nir_padded_[c:c+15,r:r+15]
        indexes_[samples,0] = c
        indexes_[samples,1] = r
        samples = samples +1

  return samples, patches_, indexes_

def inputs(eval_data):

  reshaped_image = tf.cast(eval_data, tf.float32)

  return reshaped_image



def calcNDVI(rgb, nir):


  num = cv2.subtract(nir.astype(float),rgb.astype(float))
  den = cv2.add(nir.astype(float),rgb.astype(float))
  ndvi = cv2.divide(num,den)
 
  cv2.normalize(ndvi,ndvi,0,255,cv2.NORM_MINMAX)

  for c in xrange(0, ndvi.shape[0]):
    for r in xrange(0, ndvi.shape[1]):
      if(nir[c,r] <= 50):
        ndvi[c,r] = 0

  return ndvi.astype(np.uint8)

class image_converter:

  def __init__(self):
     self.pub_seg = rospy.Publisher('/segmented_image', Image)
     self.pub_ndvi = rospy.Publisher('/ndvi_image', Image)
     self.pub_rgb = rospy.Publisher('/rgb_image', Image)
     self.pub_nir = rospy.Publisher('/nir_image', Image)
     self.bridge = CvBridge()
     self.rgb_sub = message_filters.Subscriber("/stereo_rgb/right/image_raw_sync", Image)
     self.nir_sub = message_filters.Subscriber("/stereo_nir/right/image_raw_sync", Image)

     ts = message_filters.TimeSynchronizer([self.rgb_sub, self.nir_sub], 10)
     ts.registerCallback(self.callback)


  def callback(self, rgb, nir):

    global seg_graph
    global sess
    global indexes_patches
    global logits
    global patches
    global labels

    cv_image_rgb = self.bridge.imgmsg_to_cv2(rgb, "bgr8")
    cv_image_nir = self.bridge.imgmsg_to_cv2(nir, "mono8")

    cv_image_rgb_res = cv2.resize(cv_image_rgb, None, fx=0.48, fy=0.48, interpolation = cv2.INTER_CUBIC)
    cv_image_nir_res = cv2.resize(cv_image_nir, None, fx=0.48, fy=0.48, interpolation = cv2.INTER_CUBIC)

    cv_image_rgb_res_whit, cv_image_nir_res_whit = image_whitening(cv_image_rgb_res, cv_image_nir_res)        

    #cv2.imshow("ndvi", cv_image_rgb_res)
    #cv2.imshow("...",cv_image_nir_res)
    #cv2.imshow("segmented image", seg_img_)
    #cv2.waitKey(50)

    cv_image_ndvi = calcNDVI(cv_image_rgb_res[:,:,2], cv_image_nir_res[:,:])
    #ret,thresh1 = cv2.threshold(cv_image_ndvi,150,255,cv2.THRESH_BINARY)

    #cv2.imshow("segmented image", cv_image_ndvi)
    #cv2.waitKey(50)

    samples_, patches_, indexes_ = read_images(rgb_img_=cv_image_rgb_res, nir_img_=cv_image_nir_res, ndvi_img_=cv_image_ndvi, threshold=190)
    num_patches=samples_

    seg_img_ = np.zeros((464,622,1), np.uint8)

    print("%s %d" % ('NUMERO DI PATCH: ', samples_))

    with seg_graph.as_default():
      with sess.as_default():
        start_time = time.time()
        iter = 0
        while(iter <= samples_):
          if(iter + 1000> patches_.shape[0]):
            #print(iter)
            #print(patches_.shape[0]-iter)
            #print(samples_)
            last_batch = np.zeros((1000,15,15,4), np.float32)
            last_batch[0:(patches_.shape[0]-iter),:,:,:] = patches_[iter:patches_.shape[0],:,:,:]

            last_indexes = np.zeros((1000,2), np.int16)
            last_indexes[0:(indexes_.shape[0]-iter),:] = indexes_[iter:indexes_.shape[0],:]
            #print(last_indexes[0,:])
            #print(indexes_[iter,:])
            temp = sess.run( [logits, indexes_patches], feed_dict={patches: last_batch, labels: last_indexes})

            ### WRITING ON OUTPUT IMAGE ###
            for iterissimo in xrange(0, temp[0].shape[0]):

              temp_logits = temp[0][iterissimo]
              temp_ind = temp[1][iterissimo]


              if(temp_logits[1] >= temp_logits[0]):
                seg_img_[temp_ind[0],temp_ind[1],0] = 255 


            iter = iter+1000
            continue
          temp = sess.run([logits, indexes_patches], feed_dict={patches: patches_[iter:(iter+1000)], labels: indexes_[iter:(iter+1000)]})
          iter = iter+1000

          ### WRITING ON OUTPUT IMAGE ###
          for iterissimo in xrange(0, temp[0].shape[0]):

            temp_logits = temp[0][iterissimo]
            temp_ind = temp[1][iterissimo]


            if(temp_logits[1] >= temp_logits[0]):
              seg_img_[temp_ind[0],temp_ind[1],0] = 255 

        print(time.time() - start_time)

        #cv2.imwrite("%s%s" % (os.path.dirname(os.path.realpath(__file__)), '/seg_img.png'), seg_img_)
        #cv2.imwrite("%s%s" % (os.path.dirname(os.path.realpath(__file__)), '/rgb_img.png'), cv_image_rgb_res)    
        self.pub_seg.publish(self.bridge.cv2_to_imgmsg(seg_img_, "mono8"))
        self.pub_ndvi.publish(self.bridge.cv2_to_imgmsg(cv_image_ndvi, "mono8"))
        self.pub_rgb.publish(self.bridge.cv2_to_imgmsg(cv_image_rgb_res, "bgr8"))
        self.pub_nir.publish(self.bridge.cv2_to_imgmsg(cv_image_nir_res, "mono8"))
        """
        cv2.imshow("ndvi", cv_image_ndvi)
        cv2.imshow("...",cv_image_rgb_res)
        cv2.imshow("segmented image", seg_img_)
        cv2.waitKey(50)
        """
    



       

def prova():
  
  global seg_graph
  global sess
  global indexes_patches
  global logits
  global patches
  global labels

  ic = image_converter()
  rospy.init_node('prova', anonymous=True)


  seg_graph=tf.Graph()
  classification_graph=tf.Graph();
  seg_graph.as_default()

  patches = tf.placeholder( tf.float32, [None, 15, 15, 4])
  labels = tf.placeholder( tf.int32, [None, 2])

  indexes_patches = tf.cast(labels, tf.int16)

  images = inputs(patches)
  logits = inference(images)

  variable_averages = tf.train.ExponentialMovingAverage(0.9999)
  variables_to_restore = {}
  for v in tf.all_variables():     
    if v in tf.trainable_variables():
      restore_name = variable_averages.average_name(v)
    else:
      restore_name = v.op.name
    variables_to_restore[restore_name] = v
  saver = tf.train.Saver(variables_to_restore)

  graph_def = tf.get_default_graph().as_graph_def()

  #with tf.Session() as sess:
  sess=tf.Session()
  ckpt = tf.train.get_checkpoint_state(checkpoint_dir_seg)
  if ckpt and ckpt.model_checkpoint_path:
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)), ckpt.model_checkpoint_path)
    saver.restore(sess, path)
    global_step = path.split('/')[-1].split('-')[-1]
  else:
    print('No checkpoint file found')
    return

  rospy.spin()


if __name__ == '__main__':
    prova()
