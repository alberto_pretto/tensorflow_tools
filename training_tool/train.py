from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import os.path
import time
import sys, getopt
import tensorflow.python.platform
from tensorflow.python.platform import gfile

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import cv2

import classification_functions
import segmentation_functions

train_path = os.path.join(segmentation_functions.Training_tool_path, 'Train_Folder')
max_steps=10000



def train_segmentation(inp_files, out_files, depth_patches):
  with tf.Graph().as_default():
    global_step = tf.Variable(0, trainable=False)

    # Get images and labels.
    images, labels = segmentation_functions.distorted_inputs(train_data=inp_files)

    # Build a Graph that computes the logits predictions from the
    # inference model.
    logits = segmentation_functions.inference(images, depth_inp_patches=depth_patches)

    # Calculate loss.
    loss = segmentation_functions.loss(logits, labels)

    # Build a Graph that trains the model with one batch of examples and
    # updates the model parameters.
    train_op = segmentation_functions.train(loss, global_step)

    # Create a saver.
    saver = tf.train.Saver(tf.all_variables())


    # Build an initialization operation to run below.
    init = tf.initialize_all_variables()

    # Start running operations on the Graph.
    sess = tf.Session(config=tf.ConfigProto(
        log_device_placement=False))
    sess.run(init)

    # Start the queue runners.
    tf.train.start_queue_runners(sess=sess)


    for step in xrange(max_steps):
      start_time = time.time()
      _, loss_value = sess.run([train_op, loss])
      duration = time.time() - start_time

      assert not np.isnan(loss_value), 'Model diverged with loss = NaN'

      if step % 10 == 0:
        num_examples_per_step = segmentation_functions.batch_size
        examples_per_sec = num_examples_per_step / duration
        sec_per_batch = float(duration)

        format_str = ('%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                      'sec/batch)')
        print (format_str % (datetime.now(), step, loss_value,
                             examples_per_sec, sec_per_batch))

      # Save the model checkpoint periodically.
      if (step+1) % 100 == 0 or (step + 1) == max_steps:
        checkpoint_path = os.path.join('Train_Folder', out_files[0])
        saver.save(sess, checkpoint_path, global_step=global_step)







def train_classification(inp_files, out_files, depth_patches):
  with tf.Graph().as_default():
    global_step = tf.Variable(0, trainable=False)

    # Get images and labels.
    images, labels = classification_functions.distorted_inputs(train_data=inp_files)

    # Build a Graph that computes the logits predictions from the
    # inference model.
    logits = classification_functions.inference(images, depth_inp_patches=depth_patches)

    # Calculate loss.
    loss = classification_functions.loss(logits, labels)

    # Build a Graph that trains the model with one batch of examples and
    # updates the model parameters.
    train_op = classification_functions.train(loss, global_step)

    # Create a saver.
    saver = tf.train.Saver(tf.all_variables())


    # Build an initialization operation to run below.
    init = tf.initialize_all_variables()

    # Start running operations on the Graph.
    sess = tf.Session(config=tf.ConfigProto(
        log_device_placement=False))
    sess.run(init)

    # Start the queue runners.
    tf.train.start_queue_runners(sess=sess)


    for step in xrange(max_steps):
      start_time = time.time()
      _, loss_value = sess.run([train_op, loss])
      duration = time.time() - start_time

      assert not np.isnan(loss_value), 'Model diverged with loss = NaN'

      if step % 10 == 0:
        num_examples_per_step = classification_functions.batch_size
        examples_per_sec = num_examples_per_step / duration
        sec_per_batch = float(duration)

        format_str = ('%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                      'sec/batch)')
        print (format_str % (datetime.now(), step, loss_value,
                             examples_per_sec, sec_per_batch))

      # Save the model checkpoint periodically.
      if (step+1) % 100 == 0 or (step + 1) == max_steps:
        checkpoint_path = os.path.join('Train_Folder', out_files[0])
        saver.save(sess, checkpoint_path, global_step=global_step)


def main(argv):  # pylint: disable=unused-argument
  
  inputfile = []
  outputfile = []
  network_type = []
  depth = 0
  try:
    opts, args = getopt.getopt(argv,"hi:o:d:t:",["ifile=","ofile="])
  except getopt.GetoptError:
    print('test.py -d <depth> -t <type> -i <inputfile1> -i <inputfile2> ... -o <outputfile1> -o <outputfile2>  ...')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('test.py -d <depth> -t <type> -i <inputfile1> -i <inputfile2> ... -o <outputfile1> -o <outputfile2>  ...')
      sys.exit()
    elif opt in ("-i", "--ifile"):
      inputfile.append(arg)
    elif opt in ("-o", "--ofile"):
      outputfile.append(arg)
    elif opt in ("-d", "--ofile"):
      depth = arg
    elif opt in ("-t", "--ofile"):
      network_type.append(arg)

  if(len(inputfile) == 0 or len(outputfile) != 1 or depth == 0 or len(network_type) != 1):
    print(' ')
    print(' ')
    print('usage: ')
    print('test.py -d <depth> -t <type> -i <inputfile1> -i <inputfile2> ... -o <outputfile> ...') 
    print('')
    print('-----------')
    print('')
    print('inputfile1, inputfile2, ..., are the name of .bin files used as inputs for the training phase ')
    print('outputfile is the name of the final trained weights of the network ')
    print('depth is the depth of the patches which the network use for the training phase ')
    print('type is the type of network:')
    print(' ')
    print('   segmentation: for generating a vegetation mask starting from rgb+nir patches')
    print(' ')
    print('   classification: for generating a classification mask starting from rgb+nir patches')
    print(' ')
    print(' ')
    sys.exit() 
    
  if(network_type[0] == 'segmentation'):
    train_segmentation(inp_files=inputfile, out_files=outputfile, depth_patches=depth) 
    print('cazzo')
  elif(network_type[0] == 'classification'):
    train_classification(inp_files=inputfile, out_files=outputfile, depth_patches=depth) 


if __name__ == '__main__':
  if os.path.exists(train_path)==0:
    gfile.MakeDirs(train_path)  
  main(sys.argv[1:])
