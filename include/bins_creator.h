#pragma once

#include <vector>
#include <string>
#include <cstdio>

#include <opencv2/opencv.hpp>

class BinsCreator
{
public:
  BinsCreator( int num_classes, int patch_size, double scale = 1.0 );

  bool createBins( const std::vector< std::string > &names, const std::vector< std::string > &image_paths,
                   const std::string &masks_path, int avg_num_patches, int max_bin_size_mb,
                   const std::string &base_name, const std::string &output_path = std::string(".") ) const;

  bool createGridBin( const std::string &name, const std::vector< std::string > &image_paths,
                      const std::string &mask_path, cv::Size grid_size,
                      const std::string &output_name, const std::string &output_path = std::string(".") ) const;
private:

  void rescaleImage( cv::Mat& img ) const;
  uint64_t computeMaxTotalSize( const std::vector< std::string > &names,
                                const std::vector< std::string > &images_path,
                                int avg_num_patches ) const;

  bool countNumSamplesPerImage( const std::vector< std::string > &names,
                                const std::string &masks_path, int avg_num_patches,
                                std::vector< std::vector<uint64_t> > &samples_per_image ) const;

  inline bool appendPatch( FILE *stream, uchar c, const std::vector< cv::Mat > &channels ) const;

  bool openStreams( const std::string &base_name, const std::string &output_path,
                    int num_bins, std::vector<FILE *> &bin_streams ) const;
  void closeStreams( const std::vector<FILE *> &bin_streams ) const;

  void mask2coordinates( const cv::Mat& mask, std::vector< cv::Point >& coords ) const;

  void sampleCoordinates( const std::vector< cv::Point >& src, int n_samples,
                          std::vector< cv::Point >& dst ) const;
  int num_classes_, patch_size_;
  double scale_;
};
