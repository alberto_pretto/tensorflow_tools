#pragma once

#include <opencv2/opencv.hpp>

void addBorders( const cv::Mat &src, cv::Mat &dst, int border_size );