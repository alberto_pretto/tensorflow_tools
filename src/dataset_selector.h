#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

class DatasetSelector
{
public:
  DatasetSelector();

  void setCardinality( int k ) { k_ = k; };
  void setCoverageThreshold( double thresh ){ coverage_thresh_ = thresh; };
  void select( const std::vector< cv::Mat > &histograms, std::vector<int> &selected_indices );

private:
    
  void computeCosineSimilarityMatrix ( const std::vector< cv::Mat >& histograms );
  void computeInverseFrequency( const std::vector< cv::Mat >& histograms,
                                std::vector< double >&inverse_freq );
  double getMaxSimilarity( int index );
  double getSimilarity( int index, const std::vector< int >& selected_indices );
  double getCoverage( const std::vector< int >& selected_indices, int new_index );

  int k_;
  double coverage_thresh_;
  cv::Mat csm_;
  std::vector<double> coverage_, saturated_coverage_;
};