#include <opencv2/opencv.hpp>
#include <iostream>
#include <sstream>
#include <map>
#include<boost/program_options.hpp>

#include "file_tools.h"

using namespace cv;
using namespace std;
namespace po = boost::program_options;


int main( int argc, char** argv )
{
  string app_name ( argv[0] );
  string list_fn, files_directory, output_fn;
  int cardinality = 50;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "list_fn,i", po::value<string > ( &list_fn )->required(), "List file name" )
  ( "files_directory,d", po::value<string > ( &files_directory)->required(), "Files directory" )
  ( "cardinality,n", po::value<int> ( &cardinality ), "Cardinality" )
  ( "output_fn,o", po::value<string>(&output_fn)->required(), "Output file name" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm ); // throws on error, so do after help in case
    // there are any problems
  }
  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }
  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "List file name : "<<list_fn<<endl
       << "Files directory : "<<files_directory<<endl
       << "Output file : "<<output_fn<<endl;

  // Read the features files
  std::vector< std::string> names;
  readFileNames(list_fn, names );

  files_directory += "/";

  vector <string> stripped_names;

  stripExtension(names, ".bin", stripped_names);

  // Add the base directory to each name
  for ( uint i = 0; i < names.size(); i++ )
    names[i] = files_directory + stripped_names[i] + ".txt";


  cv::Mat res, softmax, log_softmax;
  std::vector< std::pair<int,int> > res_map;
  readSamples( names, res, res_map );

  cv::Mat entropy( res.rows, 1, cv::DataType< double >::type );
  exp(res, softmax);
  for( int r = 0; r < softmax.rows; r++ )
  {
    Scalar s = sum(softmax.row(r));
    softmax.row(r) /= s.val[0];
  }

  log(softmax, log_softmax );
  multiply(softmax, log_softmax, res );
  for( int r = 0; r < softmax.rows; r++ )
  {
    Scalar s = -sum(res.row(r));
    entropy.at<double>(r,0) = s.val[0];
  }

  multimap<double, int> avg_ent;
  for( uint i = 0; i < res_map.size(); i++ )
  {
    Scalar s = sum(entropy(Rect(0, res_map[i].first, 1, res_map[i].second )));
    avg_ent.insert(pair<double,int>(s.val[0]/double(res_map[i].second),i));
  }

  std::ofstream outfile;

  outfile.open ( output_fn );

  auto it = avg_ent.rbegin();
  for( int i = 0; i < cardinality && it != avg_ent.rend(); i++, it++)
  {
    if( i )
      outfile<<std::endl;
    outfile<<stripped_names[it->second];
    outfile<<".png";
    // cout<<it->first<<" "<<it->second<<" "<<stripped_names[it->second]<<endl;
  }

  outfile.close();

  return 0;
}
