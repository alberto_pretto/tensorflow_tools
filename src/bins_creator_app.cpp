#include <iostream>
#include <opencv2/opencv.hpp>
#include<boost/program_options.hpp>

#include "file_tools.h"
#include "bins_creator.h"

namespace po = boost::program_options;
using namespace std;

int main ( int argc, char** argv )
{
  string app_name( argv[0] ), images_list_file, masks_path, output_base_name, output_path(".");
  vector<string> image_paths;
  int patch_size = 15, avg_num_patches = 50, num_classes, max_bin_size_mb = 100;
  double image_scale = 0.48;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "images_list,i", po::value<string > ( &images_list_file )->required(),
    "Images list file" )
  ( "images_path,d", po::value<vector<string> >(&image_paths)->required(),
    "Images path (can be specified multiple times)")
  ( "masks_path,m", po::value<string> (&masks_path)->required(),
    "Masks path")
  ( "image_scale,s", po::value<double>(&image_scale),
    "Image scale factor (e.g., 0.5 scale the image to 50\% of its original size)" )
  ( "num_classes,c", po::value<int>(&num_classes)->required(), "Number of classes" )
  ( "patch_size,p", po::value<int>(&patch_size), "Patch size in pixels" )
  ( "num_patches,n", po::value<int>(&avg_num_patches),
    "Average number of patches for each class to be extracted for each image" )
  ( "bin_size,b", po::value<int>(&max_bin_size_mb), "Output bin file max size in MB" )
  ( "output_base_name,o", po::value<string>(&output_base_name)->required(), "Output base name" )
  ( "output_path,l", po::value<string>(&output_path), "Output path" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "Loading file names from : "<<images_list_file<< endl;
  cout << "Images paths : "<< endl;
  for( uint i = 0; i < image_paths.size(); i++ )
    cout <<i + 1<<") "<<image_paths[i]<< endl;
  cout << "Masks path : "<<masks_path<< endl;
  cout << "Imgage scale factor: "<<image_scale<< endl;
  cout << "Number of classes : "<<num_classes<< endl;
  cout << "Patch size : "<<patch_size<< endl;
  cout << "Average number of patches for each class to be extracted for each image : "<<avg_num_patches<< endl;
  cout << "Output bin file max size : "<<max_bin_size_mb<< endl;
  cout << "Output bin base name : "<<output_base_name<< endl;
  cout << "Output bin path : "<<output_path<< endl;

  vector<string> file_names;
  if ( !readFileNames ( images_list_file, file_names ) )
    return -1;
  
  BinsCreator bc(num_classes, patch_size, image_scale );
  bc.createBins( file_names, image_paths, masks_path, avg_num_patches,
                 max_bin_size_mb, output_base_name, output_path );

  return 0;
}
