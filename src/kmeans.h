#pragma once

#include <opencv2/opencv.hpp>

class KMeans
{
public:

  KMeans() : k_(2){};
  ~KMeans(){};

  void setNumClusters( int k ){ k_ = std::max( 2, k ); };
  int numClusters(){ return k_; };

  void setSamples( const cv::Mat &samples );
  void process();
  const cv::Mat& getLabels(){ return labels_; };
  const cv::Mat& getCentroids(){ return centroids_; };
  void load( std::string filename );
  void save( std::string filename );

private:

  int k_;
  cv::Mat samples_, centroids_, labels_;

};

void testKMeans( int num_clusters, int samples_dim, int samples_size );