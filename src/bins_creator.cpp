#include "bins_creator.h"
#include "image_tools.h"

using namespace std;
using namespace cv;

BinsCreator::BinsCreator( int num_classes, int patch_size, double scale ) :
  num_classes_(num_classes),
  patch_size_(patch_size),
  scale_(scale)
{
  // Randomize
  srand ( time ( 0 ) );
}

bool BinsCreator::createBins( const vector< string >& names, const vector< string >& image_paths,
                              const string& masks_path, int avg_num_patches, int max_bin_size_mb,
                              const string& base_name, const string& output_path ) const
{
  vector< vector<uint64_t> > samples_per_image;
  if( !countNumSamplesPerImage( names, masks_path, avg_num_patches, samples_per_image ) )
    return false;

  uint64_t max_total_size = computeMaxTotalSize(names, image_paths, avg_num_patches );
  if( !max_total_size ) return false;

  uint64_t max_bin_size = uint64_t(max_bin_size_mb)*1048576;
  uint num_bins = uint(ceil(double(max_total_size)/double(max_bin_size)));
  std::vector< FILE* > bin_streams;
  if( !openStreams ( base_name, output_path, num_bins, bin_streams ) )
    return false;

  int stream_index = 0;

  for( uint i_img = 0; i_img < names.size(); i_img++ )
  {
    if(!(i_img%10))
      cout<<"Processing image "<<i_img<<" of "<<names.size()<<endl;

    // Open the mask
    string mask_filename(masks_path);
    mask_filename += "/";
    mask_filename += names[i_img];
    Mat mask = imread(mask_filename.c_str(), IMREAD_GRAYSCALE );
    if( scale_ != 1.0 )
      rescaleImage( mask );

    if( mask.empty() )
    {
      cerr << "ERROR loading mask " << mask_filename << endl;
      return false;
    }

    // From the mask file, select the sample coordinates for each class
    vector< vector< Point > > class_coords( num_classes_);
    for( int cl = 0; cl < num_classes_; cl++ )
    {
      Mat single_channel_mask = (mask == cl);
      vector< Point > tmp_coords;
      mask2coordinates( single_channel_mask, tmp_coords );
      sampleCoordinates(tmp_coords, samples_per_image[i_img][cl], class_coords[cl]);
    }

    // Extract all the channels
    vector< Mat > img_channels;
    for( uint j = 0; j < image_paths.size(); j++ )
    {
      string img_filename( image_paths[j]);
      img_filename += "/";
      img_filename += names[i_img];
      Mat img = imread(img_filename.c_str(), IMREAD_UNCHANGED );
      if( scale_ != 1.0 )
        rescaleImage( img );

      if( img.empty() )
      {
        cerr << "ERROR loading image " << img_filename << endl;
        closeStreams(bin_streams);
        return false;
      }
//       for( int cl = 0; cl < num_classes_; cl++ )
//       {
//         if( class_coords[cl].size() )
//         {
//           Point p1 = class_coords[cl][0], p2 = class_coords[cl][class_coords[cl].size() - 1];
//           p1 -=  Point (patch_size_/2, patch_size_/2);
//           p2 -=  Point (patch_size_/2, patch_size_/2);
//           Rect rect1(p1.x, p1.y, patch_size_, patch_size_);
//           Rect rect2(p2.x, p2.y, patch_size_, patch_size_);
//           Mat dbg_img = img.clone();
//           rectangle(dbg_img,rect1, Scalar(0,0,255), 1);
//           rectangle(dbg_img,rect2, Scalar(0,0,255), 1);
//           imshow("dbg_img", dbg_img);
//           while (27 != waitKey());
//         }
//       }

      if( img.channels() > 1 )
      {
        vector<Mat> tmp_channels;
        split(img, tmp_channels);
        for( auto &m : tmp_channels )
          img_channels.push_back(m);
      }
      else
        img_channels.push_back(img);
    }

    Point p_hpatch(patch_size_/2, patch_size_/2);

    vector< Mat > patch_channels(img_channels.size());
    for( int cl = 0; cl < num_classes_; cl++ )
    {
      // Extract the patches
      for( auto &p: class_coords[cl] )
      {
        Point p0 = p - p_hpatch;
        Rect rect(p0.x, p0.y, patch_size_, patch_size_);
        for( uint i = 0; i < img_channels.size(); i++ )
          patch_channels[i] = Mat(img_channels[i], rect );

        if( !appendPatch ( bin_streams[stream_index++], cl, patch_channels ) )
        {
          closeStreams( bin_streams );
          return false;
        }
        stream_index %= bin_streams.size();
      }
    }
  }

  closeStreams( bin_streams );
  return true;
}

bool BinsCreator::createGridBin ( const string& name, const vector< string >& image_paths,
                                  const string& mask_path, Size grid_size, const string& output_name,
                                  const string& output_path ) const
{
  stringstream sstr;
  sstr<<output_path;
  sstr<<"/";
  sstr<<output_name;

  FILE *bin_stream = fopen (sstr.str().c_str(), "wb");
  if( bin_stream == NULL )
  {
    cerr << "ERROR openig file " << sstr.str() << endl;
    return false;
  }

  // Open the mask
  string mask_filename(mask_path);
  mask_filename += "/";
  mask_filename += name;
  Mat mask = imread(mask_filename.c_str(), IMREAD_GRAYSCALE );
  if( scale_ != 1.0 )
    rescaleImage( mask );

  if( mask.empty() )
  {
    cerr << "ERROR loading mask " << mask_filename << endl;
    return false;
  }

  int border_size = patch_size_/2;
  int active_width = mask.cols - 2*border_size, active_height = mask.rows - 2*border_size;

  int r_step, c_step;
  if( active_height > grid_size.height )
    r_step = active_height / grid_size.height;
  else
    r_step = 1;

  if( active_width > grid_size.width )
    c_step = active_width / grid_size.width;
  else
    c_step = 1;

  vector< Point > coords;
  coords.reserve( grid_size.width * grid_size.height );

  for( int r = border_size, i_r = 0; i_r < grid_size.height; r += r_step, i_r++ )
    for(int c = border_size, i_c = 0; i_c < grid_size.width; c += c_step, i_c++ )
      coords.push_back(Point(c,r));


  // Extract all the channels
  vector< Mat > img_channels;
  for( uint j = 0; j < image_paths.size(); j++ )
  {
    string img_filename(image_paths[j]);
    img_filename += "/";
    img_filename += name;
    Mat img = imread(img_filename.c_str(), IMREAD_UNCHANGED );
    if( scale_ != 1.0 )
      rescaleImage( img );

    if( img.empty() )
    {
      cerr << "ERROR loading image " << img_filename << endl;
      fclose(bin_stream);
      return false;
    }

//     for (auto p : coords )
//     {
//       p -=  Point (patch_size_/2, patch_size_/2);
//       Rect rect(p.x, p.y, patch_size_, patch_size_);
//       Mat dbg_img = img.clone();
//       rectangle(dbg_img,rect, Scalar(0,0,255), 1);
//       imshow("dbg_img", dbg_img);
//       while (27 != waitKey());
//     }

    if( img.channels() > 1 )
    {
      vector<Mat> tmp_channels;
      split(img, tmp_channels);
      for( auto &m : tmp_channels )
        img_channels.push_back(m);
    }
    else
      img_channels.push_back(img);
  }

  Point p_hpatch(patch_size_/2, patch_size_/2);

  vector< Mat > patch_channels(img_channels.size());

  // Extract the patches
  for( auto p: coords )
  {
    p -= p_hpatch;
    Rect rect(p.x, p.y, patch_size_, patch_size_);
    for( uint i = 0; i < img_channels.size(); i++ )
      patch_channels[i] = Mat(img_channels[i], rect );

    if( !appendPatch ( bin_stream, mask.at<uchar>(p.y,p.x), patch_channels ) )
    {
      fclose( bin_stream );
      return false;
    }
  }

  fclose( bin_stream );
  return true;
}

void BinsCreator::rescaleImage ( Mat& img ) const
{
  Mat dst;
  resize( img, dst, Size(), scale_, scale_, INTER_NEAREST );
  img = dst;
}

uint64_t BinsCreator::computeMaxTotalSize ( const vector< string >& names, const vector< string > &images_path,
                                            int avg_num_patches ) const
{
  int pixel_depth = 0;
  const int mask_depth = 1;

  for( auto full_name : images_path )
  {
    full_name += "/";
    full_name += names[0];
    Mat img = imread(full_name.c_str(), IMREAD_UNCHANGED );
    if( scale_ != 1.0 )
      rescaleImage( img );

    if( img.empty() )
    {
      cerr << "ERROR loading image " << full_name  << endl;
      return 0;
    }

    pixel_depth += img.channels();
  }

  return  patch_size_*patch_size_*pixel_depth*names.size()*num_classes_*avg_num_patches +
          mask_depth*names.size()*num_classes_*avg_num_patches;
}

bool BinsCreator::countNumSamplesPerImage ( const vector< string >& names, const string& masks_path, int avg_num_patches,
                                            vector< vector< uint64_t > >& samples_per_image ) const
{
  samples_per_image.resize(names.size());
  vector<uint64_t> tot_samples(num_classes_,0);
  int border_size = patch_size_/2;

  for( uint i = 0; i < names.size(); i++ )
  {
    string full_name(masks_path);
    full_name += "/";
    full_name += names[i];
    Mat mask = imread(full_name.c_str(), IMREAD_GRAYSCALE );
    if( scale_ != 1.0 )
      rescaleImage( mask );

    if( mask.empty() )
    {
      cerr << "ERROR loading mask " << full_name << endl;
      return false;
    }

    Mat cropped_mask(mask, Rect(border_size, border_size,
                                mask.cols - 2*border_size, mask.rows - 2*border_size ) );

    samples_per_image[i].resize(num_classes_);
    for( int j = 0; j < num_classes_; j++ )
    {
      Mat single_channel_mask = (cropped_mask == j);
      samples_per_image[i][j] = countNonZero(single_channel_mask);
      tot_samples[j] += samples_per_image[i][j];
    }
//     Mat dbg_mask, dbg_cropped_mask;
//     normalize(mask, dbg_mask, 255,0,NORM_MINMAX);
//     normalize(cropped_mask, dbg_cropped_mask, 255,0,NORM_MINMAX);
//     imshow("mask", dbg_mask);
//     imshow("cropped_mask", dbg_cropped_mask);
//     while (27 != waitKey());
  }

  const uint64_t min_samples = names.size()*avg_num_patches;
  cout<<"Num samples per class: "<<min_samples<<endl;

  for( int j = 0; j < num_classes_; j++ )
  {
    //cout<<"class : "<<j<<" samples : "<<tot_samples[j]<<endl;
    if( tot_samples[j] < min_samples )
    {
      cerr << "Not enough patches per class"<<endl;
      return false;
    }
  }

  for( uint i = 0; i < names.size(); i++ )
  {
    for( int j = 0; j < num_classes_; j++ )
    {
      double class_samples = double(min_samples)*(double(samples_per_image[i][j])/double(tot_samples[j]));
      if( class_samples > 0 )
        samples_per_image[i][j] = uint64_t(ceil(class_samples));
      else
        samples_per_image[i][j] = 0;
//       cout<<samples_per_image[i][j]<<" ";
    }
//     cout<<endl;
  }
  return true;
}

bool BinsCreator::openStreams ( const string& base_name, const string& output_path,
                                int num_bins, std::vector< FILE* > &bin_streams ) const
{
  bin_streams.resize(num_bins);
  char bin_name[output_path.length() + base_name.length() + 16];
  // Open the bin files
  for( int i = 0; i < num_bins; i++ )
  {
    sprintf(bin_name,"%s/%s%.4d.bin",output_path.c_str(),base_name.c_str(),i);
    bin_streams[i] = fopen (bin_name, "wb");
    if( bin_streams[i] == NULL )
    {
      cerr << "ERROR openig file " << bin_name << endl;
      closeStreams( bin_streams );
      return false;
    }
  }
  return true;
}

void BinsCreator::closeStreams ( const vector< FILE* >& bin_streams ) const
{
  for( auto &f : bin_streams )
    if( f != NULL )
      fclose(f);
}

bool BinsCreator::appendPatch ( FILE* stream, uchar c, const vector< Mat >& channels ) const
{
  if( c != fputc(c, stream) )
    return false;

  for( uint i = 0; i < channels.size(); i++ )
  {
    for( int r = 0; r < patch_size_; r++ )
      if( patch_size_ != int(fwrite ( (const void *)channels[i].ptr<uchar>(r) , 1, patch_size_, stream)) )
      {
        cerr << "Write error"<< endl;
        return false;
      }
  }
  return true;
}

void BinsCreator::mask2coordinates ( const Mat& mask, vector< Point >& coords ) const
{
  int border_size = patch_size_/2;
  int w = mask.cols - border_size, h = mask.rows - border_size;

  coords.clear();
  coords.reserve(mask.rows*mask.cols);
  for( int r = border_size; r < h; r++ )
  {
    const uchar *img_p = mask.ptr<uchar>(r);
    img_p += border_size;
    for(int c = border_size; c < w; c++, img_p++ )
    {
      if( *img_p )
        coords.push_back(Point(c,r));
    }
  }
}

void BinsCreator::sampleCoordinates ( const vector< Point >& src, int n_samples,
                                      vector< Point >& dst ) const
{
  dst.clear();
  if( !n_samples )
    return;

  if( n_samples >= int(src.size()) )
    dst = src;
  else
  {
    int step = src.size() /(n_samples + 1);
    int start = rand()%(src.size() - n_samples*step);
    dst.reserve(n_samples);
    for( int i = 0, j = start; i < n_samples; i++, j+=step )
      dst.push_back(src[j]);
  }
}