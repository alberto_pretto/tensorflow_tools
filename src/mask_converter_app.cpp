#include <iostream>
#include <opencv2/opencv.hpp>
#include<boost/program_options.hpp>

#include "file_tools.h"
#include "image_tools.h"

namespace po = boost::program_options;
using namespace std;
using namespace cv;

int main ( int argc, char** argv )
{
  string app_name( argv[0] ), masks_list_file, masks_path, output_base_name, output_path(".");

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "masks_list,i", po::value<string > ( &masks_list_file )->required(),
    "Images list file" )
  ( "masks_directory,d", po::value<string> (&masks_path)->required(),
    "Masks directory")
  ( "output_path,o", po::value<string>(&output_path), "Output path" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "Loading mask names from : "<<masks_list_file<< endl;
  cout << "Masks directory : "<<masks_path<< endl;
  cout << "Output path : "<<output_path<< endl;

  vector<string> file_names;
  if ( !readFileNames ( masks_list_file, file_names ) )
    return -1;

  for( auto &fn : file_names )
  {
    string full_input_name(masks_path), full_output_name(output_path);
    full_input_name += "/";
    full_input_name += fn;
    full_output_name += "/";
    full_output_name += fn;

    Mat img = imread( full_input_name.c_str(), cv::IMREAD_UNCHANGED );
    Mat mask( img.size(), DataType<uchar>::type, Scalar(0));
    for( int r = 0; r < img.rows; r++ )
    {
      Vec3b *img_p = img.ptr<Vec3b>(r);
      uchar *mask_p = mask.ptr<uchar>(r);
      for( int c = 0; c < img.cols; c++, img_p++, mask_p++ )
        if(*img_p == Vec3b(0,255,0))
          *mask_p = 1;
        else if(*img_p == Vec3b(0,0,255))
          *mask_p = 2;
        else if(*img_p == Vec3b(0,165,255))
          *mask_p = 3;
    }

    imwrite( full_output_name, mask );
//     Mat dbg_mask;
//     normalize(mask, dbg_mask, 255,0,NORM_MINMAX);
//     cv::imshow("img", img);
//     cv::imshow("mask", dbg_mask);
//     while (27 != cv::waitKey());
  }

  return 0;
}
