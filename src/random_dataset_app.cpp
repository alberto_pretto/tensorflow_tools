#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include<boost/program_options.hpp>

#include "file_tools.h"

namespace po = boost::program_options;
using namespace std;

// Generates max random numbers between num numbers without repetitions
void sampleIndexes ( int max, int num, vector<int> &output_indices, vector<int> &remaining_indices )
{
  max++;
  srand ( time ( 0 ) );

  vector<int> value ( max,-1 );
  vector<int> indices ( num,-1 );

  //generate random numbers:
  for ( int i=0; i < num; i++ )
  {
    bool check; //variable to check or number is already used
    int n; //variable to store the number in
    do
    {
      n = rand()%max;
      //check or number is already used:
      check=true;

      if ( value[n] == n )
        check = false;
    }
    while ( !check ); //loop until new, unique number is found
    value[n] = n; //store the generated number in the array
    indices[i] = n;
  }

  output_indices.resize ( num );

  for ( int i=0; i < num; i++ )
    output_indices[i] = value[indices[i]];

  int remaining_size = max - num + 1;
  remaining_indices.clear();
  remaining_indices.reserve( remaining_size );
  for ( int i=0; i < max ; i++ )
    if( value[i] < 0 )
      remaining_indices.push_back(i);
}

int main ( int argc, char** argv )
{
  string app_name ( argv[0] );
  int num_samples;
  string images_list_fn, output_fn, rest_output_fn;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "num_samples,n", po::value<int> ( &num_samples )->required(), "Number of sampled images" )
  ( "images_list_fn,i", po::value<string > ( &images_list_fn )->required(), "Image list file name" )
  ( "output_fn,o", po::value<string>(&output_fn)->required(), "Output file name" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm ); // throws on error, so do after help in case
    // there are any problems
  }
  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }
  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  stringstream sstr;
  sstr<<"rest_";
  sstr<<output_fn;
  rest_output_fn = sstr.str();

  cout << "Sampling "<<num_samples<<" images from "<<images_list_fn<< endl
       << "Output file : "<<output_fn<<endl
       << "Rest output file : "<<rest_output_fn<<endl;

  vector<string> image_names;
  if( !readFileNames( images_list_fn, image_names ) )
    return -1;

  vector<int> random_images, remaining_images;
  sampleIndexes( image_names.size() - 1, num_samples, random_images, remaining_images);

  ofstream output_file, rest_output_file;
  output_file.open ( output_fn );
  rest_output_file.open ( rest_output_fn );

  for ( unsigned int i=0; i < random_images.size(); i++ )
  {
    if(i)
      output_file  << endl;
    output_file << image_names[random_images[i]];
  }

  for ( unsigned int i=0; i< remaining_images.size(); i++ )
  {
    if(i)
      rest_output_file  << endl;
    rest_output_file<< image_names[remaining_images[i]];
  }

  output_file.close();
  rest_output_file.close();

  return 0;
}
