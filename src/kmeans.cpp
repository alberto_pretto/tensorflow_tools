#include "kmeans.h"

#include <ctime>
#include <vector>
#include <locale>

#include "file_tools.h"

void KMeans::process()
{
  cv::kmeans ( samples_, k_, labels_,
               cv::TermCriteria ( cv::TermCriteria::EPS+cv::TermCriteria::COUNT, 10, 1.0 ),
               3, cv::KMEANS_PP_CENTERS, centroids_ );
}

void KMeans::load ( std::string filename )
{
  filename = generateYamlFilename( filename );
  cv::FileStorage fs( filename, cv::FileStorage::READ );
  fs["centroids"] >> centroids_;
  fs["labels"] >> labels_;
}

void KMeans::save ( std::string filename )
{
  filename = generateYamlFilename( filename );
  cv::FileStorage fs( filename, cv::FileStorage::WRITE );
  fs << "centroids" << centroids_;
  fs << "labels" << labels_;
}

void KMeans::setSamples ( const cv::Mat& samples )
{
  // normalize samples
  samples_ = samples.mul( 1.0f / (1.0f + abs(samples) ) );
  samples_ = samples.clone();
}

void testKMeans ( int num_clusters, int samples_dim, int samples_size )
{
  uint64 rng_state = (uint64)time(0);
  cv::RNG rng( rng_state );

  cv::Mat samples ( samples_size, samples_dim, CV_32F );

  cv::Mat test_centroids ( num_clusters, samples_dim, CV_32F );
  std::vector< int > ground_truths;
  ground_truths.reserve ( samples_size );

  for ( int i = 0; i < num_clusters; i++ )
  {
    float *test_centroids_ptr = test_centroids.ptr<float> ( i );
    for ( int j = 0; j < samples_dim; j++ )
      *test_centroids_ptr++ = rng.uniform ( 0.0, 1.0 );
  }

  for ( int i = 0; i < samples_size; i++ )
  {
    float *samples_ptr = samples.ptr<float> ( i );
    int i_c = rng.uniform ( 0,num_clusters );
    ground_truths.push_back ( i_c );
    float *test_centroids_ptr = test_centroids.ptr<float> ( i_c );
    for ( int j = 0; j < samples_dim; j++ )
      *samples_ptr++ = *test_centroids_ptr++ + rng.gaussian ( 0.05 );
  }

  KMeans kmeans;
  kmeans.setNumClusters(num_clusters);
  kmeans.setSamples(samples);
  kmeans.process();
  cv::Mat centroids = kmeans.getCentroids();
  cv::Mat labels = kmeans.getLabels();

  std::vector<int> centroid_map;
  for ( int i = 0; i < num_clusters; i++ )
  {
    int i_map = i;
    double min_dist2 = std::numeric_limits< double >::max();
    for ( int j = 0; j < num_clusters; j++ )
    {
      float *center_ptr = test_centroids.ptr<float> ( i ),
             *centroids_ptr = centroids.ptr<float> ( j );
      double dist2 = 0.0;
      for ( int t = 0; t < samples_dim; t++ )
      {
        dist2 += ( *centroids_ptr - *center_ptr ) * ( *centroids_ptr - *center_ptr );
        centroids_ptr++;
        center_ptr++;
      }

      if ( dist2 < min_dist2 )
      {
        i_map = j;
        min_dist2 = dist2;
      }
    }
    centroid_map.push_back ( i_map );
  }

  int errors = 0;
  for ( int i = 0; i < samples_size; i++ )
  {
    int cluster_idx = labels.at<int> ( i );

    if ( cluster_idx != centroid_map.at ( ground_truths.at ( i ) ) )
      errors++;
  }

  std::cout<<"Num clusters : "<<num_clusters<<" - "
           <<errors<<" errors on "<<samples_size
           <<" samples"<<std::endl;
}

