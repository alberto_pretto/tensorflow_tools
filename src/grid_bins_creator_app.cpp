#include <iostream>
#include <opencv2/opencv.hpp>
#include<boost/program_options.hpp>

#include "file_tools.h"
#include "bins_creator.h"

namespace po = boost::program_options;
using namespace std;

int main ( int argc, char** argv )
{
  string app_name( argv[0] ), images_list_file, masks_path, output_path(".");
  vector<string> image_paths;
  int patch_size = 15, grid_width = 32, grid_height = 24, num_classes;
  double image_scale = 0.48;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "images_list,i", po::value<string > ( &images_list_file )->required(),
    "Images list file" )
  ( "images_path,d", po::value<vector<string> >(&image_paths)->required(),
    "Images path (can be specified multiple times)")
  ( "masks_path,m", po::value<string> (&masks_path)->required(),
    "Masks path")
  ( "image_scale,s", po::value<double>(&image_scale),
    "Image scale factor (e.g., 0.5 scale the image to 50\% of its original size)" )
  ( "num_classes,c", po::value<int>(&num_classes)->required(), "Number of classes" )
  ( "patch_size,p", po::value<int>(&patch_size), "Patch size in pixels" )
  ( "grid_width,x", po::value<int>(&grid_width), "Grid width" )
  ( "grid_height,y", po::value<int>(&grid_height), "Grid height" )
  ( "output_path,l", po::value<string>(&output_path), "Output path" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "Loading file names from : "<<images_list_file<< endl;
  cout << "Images paths : "<< endl;
  for( uint i = 0; i < image_paths.size(); i++ )
    cout <<i + 1<<") "<<image_paths[i]<< endl;
  cout << "Masks path : "<<masks_path<< endl;
  cout << "Imgage scale factor: "<<image_scale<< endl;
  cout << "Number of classes : "<<num_classes<< endl;
  cout << "Grid size : ("<<grid_width<<" x "<<grid_height<<")"<< endl;
  cout << "Output bin path : "<<output_path<< endl;

  vector<string> file_names;
  if ( !readFileNames ( images_list_file, file_names ) )
    return -1;

  vector<std::string> stripped_names;

  stripExtension( file_names, string (".png"), stripped_names );

  string list_filename(output_path);
  list_filename += "/list.txt";

  ofstream list_file;
  list_file.open ( list_filename );

  BinsCreator bc(num_classes, patch_size, image_scale );
  for( uint i = 0; i < file_names.size(); i++ )
  {
    cout<<"Extracting grid patches from "<<file_names[i]<<endl;
    if( i )
      list_file << endl;

    stripped_names[i] += ".bin";
    if( !bc.createGridBin( file_names[i], image_paths, masks_path,
                      cv::Size(grid_width, grid_height), stripped_names[i], output_path ) )
      return -1;

    list_file << stripped_names[i];
  }

  list_file.close();

  return 0;
}
