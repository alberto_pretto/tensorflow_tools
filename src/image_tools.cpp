#include "image_tools.h"

void addBorders( const cv::Mat &src, cv::Mat &dst, int border_size )
{
  dst = cv::Mat( src.cols + 2*border_size, src.rows + 2*border_size, src.type() );
  cv::copyMakeBorder(src, dst, border_size, border_size,
                     border_size, border_size, cv::BORDER_REFLECT_101 );
}