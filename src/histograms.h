#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

void computeHistogramsFromSamples( const cv::Mat& samples, const std::vector< std::pair< int, int > >& samples_map,
                                   int hist_dim, std::vector< cv::Mat >& histograms );

// Implement me!
// void histSparse2Dense();

void saveHistograms( std::string filename, const std::vector < cv::Mat > &histograms );
void loadHistograms( std::string filename, std::vector< cv::Mat >& histograms );

void drawHistograms( const std::vector < cv::Mat > &histograms );
