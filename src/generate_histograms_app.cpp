#include <opencv2/opencv.hpp>
#include <iostream>
#include <sstream>
#include<boost/program_options.hpp>

#include "file_tools.h"
#include "histograms.h"
#include "kmeans.h"
#include "dataset_selector.h"

using namespace cv;
using namespace std;
namespace po = boost::program_options;


int main( int argc, char** argv )
{
  string app_name ( argv[0] );
  string list_fn, files_directory, output_fn;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "list_fn,i", po::value<string > ( &list_fn )->required(), "List file name" )
  ( "files_directory,d", po::value<string > ( &files_directory)->required(), "Files directory" )
  ( "output_fn,o", po::value<string>(&output_fn)->required(), "Output file name" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm ); // throws on error, so do after help in case
    // there are any problems
  }
  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }
  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "List file name : "<<list_fn<<endl
       << "Files directory : "<<files_directory<<endl
       << "Output file : "<<output_fn<<endl;

  // Read the features files
  std::vector< std::string> names;
  readFileNames(list_fn, names );

  files_directory += "/";

  vector <string> stripped_names;

  stripExtension(names, ".bin", stripped_names);

  // Add the base directory to each name
  for ( uint i = 0; i < names.size(); i++ )
    names[i] = files_directory + stripped_names[i] + ".txt";


  cv::Mat features;
  std::vector< std::pair<int,int> > features_map;
  readSamples( names, features, features_map );

  const int num_clusters = 4096;

  KMeans kmeans;
  kmeans.setNumClusters(num_clusters);
  kmeans.setSamples(features);
  kmeans.process();
  cv::Mat labels = kmeans.getLabels();

  std::vector< cv::Mat > histograms;
  computeHistogramsFromSamples( labels, features_map, num_clusters, histograms );
  saveHistograms( output_fn, histograms);
  drawHistograms( histograms );

  return 0;
}
