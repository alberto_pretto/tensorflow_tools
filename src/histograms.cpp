#include "histograms.h"

#include <iostream>
#include "file_tools.h"

void computeHistogramsFromSamples( const cv::Mat &samples, const std::vector< std::pair<int,int> > &samples_map,
                                   int hist_dim, std::vector < cv::Mat > &histograms )
{
  cv::Mat tmp_samples;
  samples.convertTo(tmp_samples, CV_32F);
  histograms.clear();
  histograms.resize(samples_map.size());

  float range[] = { 0, float(hist_dim) } ;
  const float* hist_range = { range };

//   cv::Mat cum_histogram = cv::Mat::zeros( hist_dim, 1, CV_64F ), tmp_hist;
  for( uint i = 0; i < histograms.size(); i++ )
  {
    int start_row = samples_map[i].first, end_row =  samples_map[i].first +  samples_map[i].second;
    cv::Mat samples_subset = tmp_samples.rowRange( start_row, end_row);

    cv::calcHist( &samples_subset, 1, 0, cv::Mat(), histograms[i], 1, &hist_dim, &hist_range, true, false );
//     histograms[i].convertTo(tmp_hist, CV_64F);
//     cum_histogram += tmp_hist;
  }
//   std::cout<<cum_histogram<<std::endl;
}

void loadHistograms( std::string filename, std::vector < cv::Mat > &histograms )
{
  histograms.clear();
  filename = generateYamlFilename( filename );
  cv::FileStorage fs( filename, cv::FileStorage::READ );
  int histograms_size, histogram_dim;
  fs["histograms_size"] >> histograms_size;
  fs["histogram_dim"] >> histogram_dim;

  if( !histograms_size || !histogram_dim )
    return;

  histograms.resize(histograms_size);
  cv::FileNode histograms_node = fs["histograms"];
  cv::FileNodeIterator it = histograms_node.begin(), it_end = histograms_node.end();

  for( int i = 0; it != it_end; i++, it++ )
    (*it) >> histograms[i];

  fs.release();
}

void saveHistograms( std::string filename, const std::vector < cv::Mat > &histograms )
{
  if( histograms.empty() )
    return;

  int histograms_size = histograms.size(), histogram_dim = histograms[0].rows;
  filename = generateYamlFilename( filename );
  cv::FileStorage fs( filename, cv::FileStorage::WRITE );

  fs<<"histograms_size"<<histograms_size;
  fs<<"histogram_dim"<<histogram_dim;
  fs << "histograms" << "[";
  for( int i = 0; i < histograms_size; i++ )
    fs << histograms[i];
  fs << "]";

  fs.release();
}

void drawHistograms( const std::vector < cv::Mat > &histograms )
{
  if( histograms.empty() )
    return;

  int hist_dim = histograms[0].rows;
  int hist_w = hist_dim*2;
  if( hist_w < 640 ) hist_w = 640;
  if( hist_w > 1600 ) hist_w = 1600;

  int hist_h = cvRound(double(hist_w)*9.0/16.0);
  double bin_w = double(hist_w)/hist_dim;
  cv::namedWindow("Histogram vis", CV_WINDOW_AUTOSIZE );
  int line_tick = cvRound(bin_w);
  if ( line_tick < 1 ) line_tick = 1;
  for( uint i = 0; i < histograms.size(); i++ )
  {
//     std::cout<<histograms[i]<<std::endl;
    cv::Mat hist_image( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) ), norm_hist;
    std::cout<<cv::sum(histograms[i])<<std::endl;
    cv::normalize(histograms[i], norm_hist, 0, hist_image.rows, cv::NORM_MINMAX );

    for( int j = 0; j < hist_dim; j++ )
    {
      float hist_bal = norm_hist.at<float>(j);
      if( hist_bal )
        cv::line( hist_image, cv::Point( cvRound(bin_w*(j)), hist_h - 1 ) ,
                  cv::Point( cvRound(bin_w*j), hist_h - cvRound(norm_hist.at<float>(j)) ),
                  cv::Scalar( 255, 0, 0), line_tick, 8, 0  );
    }
    cv::imshow("Histogram vis", hist_image );

    cv::waitKey(0);
  }
}