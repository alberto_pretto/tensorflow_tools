#include <iostream>
#include <vector>
#include <list>
#include <utility>
#include <sstream>
#include <cstdio>
#include <opencv2/opencv.hpp>
#include<boost/program_options.hpp>

namespace po = boost::program_options;
using namespace std;
using namespace cv;

int main ( int argc, char** argv )
{
  string app_name( argv[0] ), input_bin_filename;
  int patch_size, patch_depth;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "input_bin_filename,i", po::value<string > ( &input_bin_filename )->required(),
    "Input bin" )
  ( "patch_size,s", po::value<int>(&patch_size)->required(), "Image size" )
  ( "patch_depth,d", po::value<int>(&patch_depth)->required(), "Image size" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  vector<Mat> out_imgs(patch_depth);
  vector< list< pair<int,Mat> > > cl_patches(patch_depth);

  int w_samples = 1024/patch_size, h_samples = 768/patch_size, num_samples = w_samples*h_samples;
  int w = w_samples*patch_size, h = h_samples*patch_size;

  FILE *stream = fopen(input_bin_filename.c_str(), "rb");

  int cl;
  while(true)
  {
    if( EOF == (cl = fgetc(stream) ) )
      break;

    for( int channel = 0; channel < patch_depth; channel++)
    {
      auto &cl_patch = cl_patches[channel];
      Mat patch(patch_size, patch_size, DataType<uchar>::type );

      for( int c = 0; c < patch_size; c++ )
      {
        uchar *patch_ptr = patch.ptr<uchar>(c);
        if ( patch_size != int(fread ( (void*)patch_ptr, 1, patch_size , stream )) )
          break;
      }
      cl_patch.push_back(pair<int,Mat>(cl,patch));
    }

    if( int(cl_patches[0].size()) == num_samples )
    {
      for( int channel = 0; channel < patch_depth; channel++)
      {
        auto &cl_patch = cl_patches[channel];
        Mat &out_img = out_imgs[channel];
        out_img = Mat(h,w, DataType<uchar>::type, Scalar(0) );
        int sample_row = 0, sample_col = 0;
        for( auto it = cl_patch.begin(); it != cl_patch.end(); it++ )
        {
          stringstream sstr;
          sstr<< (it->first);
          cv::putText(it->second, sstr.str(),Point(8,12),FONT_HERSHEY_COMPLEX_SMALL,0.8, Scalar(255,255,255));
          Rect roi(sample_col*patch_size, sample_row*patch_size, patch_size, patch_size );
          it->second.copyTo(out_img(roi));
          if( ++sample_col >= w_samples )
          {
            sample_col = 0;
            ++sample_row;
          }
        }
        cl_patch.clear();
        stringstream sstr;
        sstr<<"Channel ";
        sstr<<channel;
        imshow(sstr.str(), out_img );
      }
      while (27 != waitKey());
    }
  }

  fclose(stream);
  return 0;
}
