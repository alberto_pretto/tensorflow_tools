#include "dataset_selector.h"

DatasetSelector::DatasetSelector() :
  k_(1),
  coverage_thresh_(-1)
{

}

void DatasetSelector::select ( const std::vector< cv::Mat >& histograms,
                               std::vector< int >& selected_indices )
{
  selected_indices.clear();
  int hists_size = histograms.size();
  if( hists_size <= k_ )
  {
    for( int i = 0; i < hists_size; i++ )
      selected_indices.push_back(i);
    return;
  }

  computeCosineSimilarityMatrix ( histograms );

  selected_indices.reserve(k_);
  std::vector< bool > selected_mask(hists_size,false);

  coverage_.resize( hists_size );
  saturated_coverage_.resize( hists_size );

  double alpha;
  if( coverage_thresh_ <= 0 || coverage_thresh_ > double(hists_size) )
    alpha = 1.0;
  else
    alpha = coverage_thresh_/double(hists_size);

  double max_reward = -1;
  int best_idx = -1;
  for( int i = 0; i < hists_size; i++ )
  {
    double reward = getMaxSimilarity(i);
    saturated_coverage_[i] = alpha*reward;
    if( reward > max_reward )
    {
      max_reward = reward;
      best_idx = i;
    }
  }

  selected_indices.push_back(best_idx);
  selected_mask[best_idx] = true;

  for( int i = 0; i < hists_size; i++ )
    coverage_[i] = csm_.at<float>(i,best_idx);

//   for( int i = 0; i < hists_size; i++ )
//     std::cout<<saturated_coverage_[i]<<" ";
//   std::cout<<std::endl;

  for( int i = 1; i < k_; i++ )
  {
    max_reward = -1;
    best_idx = -1;
    for( int j = 0; j < hists_size; j++ )
    {
      if( !selected_mask[j] )
      {
        double cur_reward = getCoverage( selected_indices, j );
        if( cur_reward > max_reward )
        {
          max_reward = cur_reward;
          best_idx = j;
        }
      }
    }
    selected_indices.push_back(best_idx);
    selected_mask[best_idx] = true;

    for( int i = 0; i < hists_size; i++ )
      coverage_[i] += csm_.at<float>(i,best_idx);
  }
}

// C_i(S)
double DatasetSelector::getSimilarity( int index, const std::vector< int >& selected_indices )
{
  double sim = 0.0;
  for( uint i = 0; i < selected_indices.size(); i++ )
    sim += csm_.at<float>(index, selected_indices[i]);
  return sim;
}

// C_i(V)
double DatasetSelector::getMaxSimilarity( int index )
{
  return cv::sum( csm_.row(index) )[0];
}

double DatasetSelector::getCoverage ( const std::vector< int >& selected_indices, int new_index )
{
  double coverage = 0.0;
  for( int i = 0; i < csm_.rows; i++ )
    coverage += std::min( coverage_[i] + csm_.at<float>(i,new_index), saturated_coverage_[i] );
  return coverage;
}

void DatasetSelector::computeCosineSimilarityMatrix ( const std::vector< cv::Mat >& histograms )
{
  if( histograms.empty() )
    return;

  double dbg_min = 1.0, dbg_max = 0.0;
  int hist_dim = histograms[0].rows, mat_size = histograms.size();

  std::vector< double >inverse_freq2;
  computeInverseFrequency( histograms, inverse_freq2 );
  for( uint i = 0; i < inverse_freq2.size(); i++ )
    inverse_freq2[i] *= inverse_freq2[i];

  csm_ = cv::Mat::eye( mat_size, mat_size, CV_32F );
  for( int r = 0; r < mat_size; r++ )
  {
    float *row_p = csm_.ptr<float>(r);
    row_p += (r + 1);
    for( int c = r + 1; c < mat_size; c++, row_p++ )
    {
      const float *hist_r_p = histograms[r].ptr<float>(0),
                  *hist_c_p = histograms[c].ptr<float>(0);
      double cs_den = 0.0, cs_num_1 = 0.0, cs_num_2 = 0.0;
      for( int i = 0; i < hist_dim; i++, hist_r_p++, hist_c_p++ )
      {
        double h0 = double(*hist_r_p), h1 = double(*hist_c_p);
        cs_den += h0*h1*inverse_freq2[i];
        cs_num_1 += h0*h0*inverse_freq2[i];
        cs_num_2 += h1*h1*inverse_freq2[i];
      }

      *row_p = cs_den*(1.0/sqrt(cs_num_1))*(1.0/sqrt(cs_num_2));

      if( *row_p < dbg_min )
        dbg_min = *row_p;
      if( *row_p > dbg_max )
        dbg_max = *row_p;
    }
  }

  csm_ = csm_ | csm_.t();
//   std::cout<<csm_<<std::endl;
  std::cout<<dbg_min<<" "<<dbg_max<<std::endl;
}

// Inverse document frequency, that is calculated as the logarithm of the ratio of the
// number of images where $\alpha$ appears over the total number of images N that compose the input dataset.
void DatasetSelector::computeInverseFrequency( const std::vector< cv::Mat >& histograms,
                                               std::vector< double >&inverse_freq )
{
  int hist_dim = histograms[0].rows, hists_size = histograms.size();
  inverse_freq.resize(hist_dim, 0);
  for( int i = 0; i < hists_size; i++ )
  {
    const float *hist_p = histograms[i].ptr<float>(0);
    for( int j = 0; j < hist_dim; j++, hist_p++ )
    {
      if( *hist_p > 0.0f )
        inverse_freq[j] += 1.0;
    }
  }
  double norm_factor = 1.0/double(hists_size);
  for( int j = 0; j < hist_dim; j++ )
    inverse_freq[j] = log(inverse_freq[j]*norm_factor);
}