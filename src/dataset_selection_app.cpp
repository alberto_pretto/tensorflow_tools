#include <opencv2/opencv.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include<boost/program_options.hpp>

#include "file_tools.h"
#include "histograms.h"
#include "dataset_selector.h"

using namespace cv;
using namespace std;
namespace po = boost::program_options;

std::string convertNameToImage( std::string &input )
{
  std::string output = input.substr(0, input.size() - 6);
  output += ".png";
  return output;
}

int main( int argc, char** argv )
{
  string app_name ( argv[0] );
  string hist_filename, list_fn, output_fn;
  int cardinality = 50;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "hist_filename,i", po::value<string > ( &hist_filename )->required(), "Histograms file name" )
  ( "list_fn,f", po::value<string > ( &list_fn )->required(), "List file name" )
  ( "cardinality,n", po::value<int> ( &cardinality ), "Cardinality" )
  ( "output_fn,o", po::value<string>(&output_fn)->required(), "Output file name" );

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      return 0;
    }

    po::notify ( vm ); // throws on error, so do after help in case
    // there are any problems
  }
  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }
  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    return -1;
  }

  cout << "Histograms file name : "<<hist_filename<<endl
       << "List file name : "<<list_fn<<endl
       << "Cardinality : "<<cardinality<<endl
       << "Output file : "<<output_fn<<endl;

  std::vector< std::string> file_names;
  readFileNames(list_fn, file_names );

  std::vector< cv::Mat > histograms;
  loadHistograms( hist_filename, histograms);
//   drawHistograms( histograms );

  DatasetSelector ds;
  ds.setCardinality(cardinality);
  std::vector< int > selected_indices;
  ds.select(histograms, selected_indices);

  std::ofstream outfile;

  outfile.open ( output_fn );

  for( uint i = 0; i < selected_indices.size(); i++)
  {
    if( i )
      outfile<<std::endl;
    outfile<<file_names[selected_indices[i]];
  }

  outfile.close();

  return 0;
}