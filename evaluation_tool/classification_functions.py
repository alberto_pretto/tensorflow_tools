#!/usr/bin/env python

from __future__ import print_function

import roslib 
import sys
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math
import input_bin


batch_size = 128

eval_data = 'test'
current_path = os.path.dirname(os.path.realpath(__file__))
checkpoint_dir = os.path.join(current_path, 'checkpoints')

def inference(images):

  # conv1
  with tf.variable_scope('conv1') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 4, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(images, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.0))
    bias = tf.nn.bias_add(conv, biases)
    conv1 = tf.nn.relu(bias, name=scope.name)

  # norm1
  norm1 = tf.nn.lrn(conv1, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm1')

  # conv2
  with tf.variable_scope('conv2') as scope:
    kernel = _variable_with_weight_decay('weights', shape=[5, 5, 64, 64],
                                         stddev=1e-4, wd=0.0)
    conv = tf.nn.conv2d(norm1, kernel, [1, 2, 2, 1], padding='SAME')
    biases = _variable_on_cpu('biases', [64], tf.constant_initializer(0.1))
    bias = tf.nn.bias_add(conv, biases)
    conv2 = tf.nn.relu(bias, name=scope.name)

  # norm2
  norm2 = tf.nn.lrn(conv2, 4, bias=1.0, alpha=0.001 / 9.0, beta=0.75,
                    name='norm2')


  # local3
  with tf.variable_scope('local3') as scope:
    norm2_shape = norm2.get_shape().as_list()
    dim = norm2_shape[1] * norm2_shape[2] * norm2_shape[3]
    reshape = tf.reshape(norm2, [norm2_shape[0] ,dim])

    weights = _variable_with_weight_decay('weights', shape=[dim, 384],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [384], tf.constant_initializer(0.1))

    local3 = tf.nn.relu_layer(reshape, weights, biases, name=scope.name)

  # local4
  with tf.variable_scope('local4') as scope:
    weights = _variable_with_weight_decay('weights', shape=[384, 192],
                                          stddev=0.04, wd=0.004)
    biases = _variable_on_cpu('biases', [192], tf.constant_initializer(0.1))

    local4 = tf.nn.relu_layer(local3, weights, biases, name=scope.name)

  # softmax, i.e. softmax(WX + b)
  with tf.variable_scope('softmax_linear') as scope:
    weights = _variable_with_weight_decay('weights', [192, 3],
                                          stddev=1/192.0, wd=0.0)
    biases = _variable_on_cpu('biases', [3],
                              tf.constant_initializer(0.0))
    softmax_linear = tf.nn.xw_plus_b(local4, weights, biases, name=scope.name)

  return softmax_linear



def _variable_on_cpu(name, shape, initializer):
  with tf.device('/cpu:0'):
    var = tf.get_variable(name, shape, initializer=initializer)
  return var


def _variable_with_weight_decay(name, shape, stddev, wd):
  var = _variable_on_cpu(name, shape,
                         tf.truncated_normal_initializer(stddev=stddev))
  if wd:
    weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name='weight_loss')
    tf.add_to_collection('losses', weight_decay)
  return var



def _generate_image_and_label_batch_classification(image, label):
  # Create a queue that shuffles the examples, and then
  # read 'FLAGS.batch_size' images + labels from the example queue.
  num_preprocess_threads = 16
  images, label_batch = tf.train.batch(
      [image, label],
      batch_size=batch_size,
      num_threads=num_preprocess_threads,
      capacity=batch_size)


  return images, tf.reshape(label_batch, [batch_size])


def read_inputs(train_data):
  filenames = []
  for iter in xrange(0, len(train_data)):
    filenames.append(os.path.join(current_path, train_data[iter]))
  for f in filenames:
    if not gfile.Exists(f):
      raise ValueError('Failed to find file: ' + f)

  # Create a queue that produces the filenames to read.
  filename_queue = tf.train.string_input_producer(filenames)

  # Read examples from files in the filename queue.
  read_input = input_bin.read_bin_file_classification(filename_queue)
  reshaped_image = tf.cast(read_input.uint8image, tf.float32)

  # Subtract off the mean and divide by the variance of the pixels.
  float_image = tf.image.per_image_whitening(reshaped_image)

  # Generate a batch of images and labels by building up a queue of examples.
  return _generate_image_and_label_batch_classification(float_image, read_input.label)























class image_converter:

  def __init__(self):
     #self.pub_class = rospy.Publisher('classification_image', Image)
     self.bridge = CvBridge()
     self.rgb_sub = message_filters.Subscriber("rgb_image", Image)
     self.nir_sub = message_filters.Subscriber("nir_image", Image)
     self.seg_sub = message_filters.Subscriber("segmented_image", Image)

     ts = message_filters.TimeSynchronizer([self.rgb_sub, self.nir_sub, self.seg_sub], 10)
     ts.registerCallback(self.callback)


  def callback(self, rgb, nir, seg):

    global seg_graph
    global sess
    global indexes_patches
    global logits
    global patches
    global labels

    cv_image_rgb = self.bridge.imgmsg_to_cv2(rgb, "bgr8")
    cv_image_nir = self.bridge.imgmsg_to_cv2(nir, "mono8")
    cv_image_seg = self.bridge.imgmsg_to_cv2(seg, "mono8")

    cv_image_rgb_res = cv2.resize(cv_image_rgb, None, fx=1, fy=1, interpolation = cv2.INTER_CUBIC)
    cv_image_nir_res = cv2.resize(cv_image_nir, None, fx=1, fy=1, interpolation = cv2.INTER_CUBIC)

    temp_rgb = cv_image_rgb_res.copy()

    """
    cv2.imshow("ndvi", cv_image_rgb_res)
    cv2.imshow("...",cv_image_nir_res)
    cv2.imshow("segmented image", cv_image_seg)
    cv2.waitKey(50)
    """

    cv_image_rgb_res = cv2.cvtColor(cv_image_rgb_res, cv2.COLOR_BGR2RGB)


    #cv2.imshow("ndvi", temp)
    #cv2.imshow("ndvi2", cv_image_rgb_res)
    #cv2.waitKey(100)
    cv_image_rgb_res_whit, cv_image_nir_res_whit = image_whitening(cv_image_rgb_res, cv_image_nir_res)        
    
    detected_blobs_ = FindBlobs(cv_image_seg)
    blobs_ = BlobsSampling(blobs=detected_blobs_)
    samples_class, patches_, indexes_ = read_images_sampled(rgb_img = cv_image_rgb_res_whit, nir_img = cv_image_nir_res_whit, veg_mask_=blobs_)    



    #cv2.imshow("rgb", cv_image_rgb_res)
    #cv2.imshow("nir", cv_image_nir_res)
    #cv2.imshow("seg", cv_image_seg)
    #cv2.waitKey(100)

    
    class_img_ = np.zeros((464,622,3), np.uint8)

    print("%s %d" % ('NUMBER OF PATCH: ', samples_class))

    with class_graph.as_default():
      with sess.as_default():
        start_time = time.time()
        iter = 0
        
        while(iter < samples_class):
          #print(iter)
          temp = sess.run([logits, indexes_patches], feed_dict={patches: patches_[iter:(iter+20)], labels: indexes_[iter:(iter+20)]})
          iter = iter+20

          ### WRITING ON OUTPUT IMAGE ###
          for iterissimo in xrange(0, temp[0].shape[0]):

            temp_logits = temp[0][iterissimo]
            temp_ind = temp[1][iterissimo]

            #print(temp_logits)

            if(temp_logits[1] > temp_logits[0] and temp_logits[1] > temp_logits[2]):
              class_img_[temp_ind[0],temp_ind[1],0] = 255  
            elif(temp_logits[0] > temp_logits[1] and temp_logits[0] > temp_logits[2]):
              class_img_[temp_ind[0],temp_ind[1],1] = 255   
            elif(temp_logits[2] > temp_logits[1] and temp_logits[2] > temp_logits[0]):
              class_img_[temp_ind[0],temp_ind[1],2] = 255  


        
        print(time.time()-start_time)
        output = OutputImages(rgb_img = temp_rgb, sampled_mask_=blobs_, det_blobs_=detected_blobs_, class_img_=class_img_)
        
        #cv2.imshow("class_img", class_img_)
        #cv2.imshow("...",cv_image_rgb_res)
        #cv2.imshow("....", cv_image_nir_res)
        #cv2.imshow("segmented", cv_image_seg)
        #cv2.imshow("segmented image", output)
        cv2.imshow("output image", output)
        cv2.waitKey(50)
        
        
    



       

def prova():
  
  global class_graph
  global sess
  global indexes_patches
  global logits
  global patches
  global labels

  ic = image_converter()
  rospy.init_node('prova', anonymous=True)

  
  class_graph=tf.Graph()
  class_graph.as_default()

  patches = tf.placeholder( tf.float32, [None, 61, 61, 4])
  labels = tf.placeholder( tf.int32, [None, 2])

  indexes_patches = tf.cast(labels, tf.int16)

  images = inputs(patches)
  logits = inference_class(images)

  variable_averages = tf.train.ExponentialMovingAverage(0.9999)
  variables_to_restore = {}
  for v in tf.all_variables():     
    if v in tf.trainable_variables():
      restore_name = variable_averages.average_name(v)
    else:
      restore_name = v.op.name
    variables_to_restore[restore_name] = v
  saver = tf.train.Saver(variables_to_restore)

  graph_def = tf.get_default_graph().as_graph_def()

  #with tf.Session() as sess:
  sess=tf.Session()
  ckpt = tf.train.get_checkpoint_state(checkpoint_dir_class)
  if ckpt and ckpt.model_checkpoint_path:
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)), ckpt.model_checkpoint_path)
    saver.restore(sess, path)
    global_step = path.split('/')[-1].split('-')[-1]
  else:
    print('No checkpoint file found')
    return
  
  rospy.spin()


if __name__ == '__main__':
    prova()
