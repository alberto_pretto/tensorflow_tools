from __future__ import print_function

import sys, getopt
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math
import input_bin
import classification_functions
from datetime import datetime

def evaluate(saver, logits, labels, top_k_op, ITERATIONS, checkpoint_file):

  global seg_graph
  with tf.Session() as sess:
    if checkpoint_file: 
      # Restores from checkpoint
      saver.restore(sess, checkpoint_file) 
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))


      iter = 0
      total_sample_count = ITERATIONS*classification_functions.batch_size
      true_count = 0.0
      start_time = time.time()
      while(iter < ITERATIONS):

        if(iter%10==0):
          print(("%s%d")%('Iteration number ', iter))

        # Evaluation
        predictions = sess.run([top_k_op])

        true_count += np.sum(predictions[0])
        iter += 1

      # Compute precision @ 1.
      print(true_count)
      print(total_sample_count)
      precision = true_count / total_sample_count
      print('%s: mean accuracy (MA) @ 1 = %.5f' % (datetime.now(), precision))

    except Exception as e:  
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=0) 

def evaluation(inp_file, checkpoints):

  global seg_graph

  seg_graph=tf.Graph()
  classification_graph=tf.Graph();
  seg_graph.as_default()

  # Get images and labels.
  images, labels = classification_functions.read_inputs(train_data=inp_file)
  logits = classification_functions.inference(images)

  # Calculate predictions
  top_k_op = tf.nn.in_top_k(logits, labels, 1)

  filename = (os.path.join(classification_functions.current_path, inp_file[0]))
  ITERATIONS = os.path.getsize(filename)/(14885*classification_functions.batch_size) + 1

  variable_averages = tf.train.ExponentialMovingAverage(0.9999)
  variables_to_restore = {}
  for v in tf.all_variables():     
    if v in tf.trainable_variables():
      restore_name = variable_averages.average_name(v)
    else:
      restore_name = v.op.name
    variables_to_restore[restore_name] = v
  saver = tf.train.Saver(variables_to_restore)

  evaluate(saver, logits, labels, top_k_op, ITERATIONS, checkpoint_file = checkpoints)


def main(argv):  # pylint: disable=unused-argument
  
  inputfile = []
  checkpoint = []

  try:
    opts, args = getopt.getopt(argv,"hi:c:",["ifile="])
  except getopt.GetoptError:
    print('test.py -i <inputfile> ')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('test.py -i <inputfile> ')
      print('')
      sys.exit()
    elif opt in ("-i", "--ifile"):
      inputfile.append(arg)
    elif opt in ("-c", "--ifile"):
      checkpoint.append(arg)  

  if(len(inputfile) != 1 or len(checkpoint)!=1):
    print('')
    print('')
    print('usage: ')
    print('test.py -i <inputfile> -c <checkpoint> ') 
    print('')
    print('-----------')
    print('')
    print('inputfile is the name of .bin files used as inputs for the evaluation phase ')
    print('checkpoint is the name of the storage file (for the network trained previously) used for the evaluation')
    print('')
    print('')
    sys.exit() 
    
  evaluation(inp_file=inputfile, checkpoints=checkpoint) 


if __name__ == '__main__':
  if os.path.exists(classification_functions.checkpoint_dir)==0:
    gfile.MakeDirs(classification_functions.checkpoint_dir)
  main(sys.argv[1:])

