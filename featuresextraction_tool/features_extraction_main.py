from __future__ import print_function

import sys, getopt
import rospy 
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import numpy as np
import cv
import time
import os
import tensorflow.python.platform.logging
import tensorflow.python.platform
from tensorflow.python.platform import gfile
import numpy as np
import tensorflow as tf
import math
import input_bin
import classification_functions
  

def evaluate_and_extract(saver, logits, labels, descriptors, ITERATIONS, out_file, checkpoint_file, binfiles):

  with tf.Session() as sess:
    if checkpoint_file:  
      # Restores from checkpoint
      saver.restore(sess, checkpoint_file) 
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))


      iter = 0
      out_file_softmax = open( os.path.join(out_file[0],'softmax', ("%s%s") % (binfiles[0:len(binfiles)-4],'txt')  ) ,"w")
      out_file_features = open( os.path.join(out_file[0],'features', ("%s%s") % (binfiles[0:len(binfiles)-4],'txt')  ) ,"w")
      while(iter < ITERATIONS):
        outputs = sess.run([ logits, descriptors])  
        length = outputs[0].shape[0]

        for i in xrange(0, length):

          
          temp_logits = outputs[0][i]
          temp_descriptors = outputs[1][i]

          for j in xrange(0, temp_logits.shape[0]):
            out_file_softmax.write(("%f")%temp_logits[j])
            out_file_softmax.write(" ")
          out_file_softmax.write("\n")

          for j in xrange(0, temp_descriptors.shape[0]):
            out_file_features.write(("%f")%temp_descriptors[j])
            out_file_features.write(" ")
          out_file_features.write("\n")
          
        iter = iter + 1
   
      out_file_softmax.close()
      out_file_features.close()

    except Exception as e:  
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=50) 



def features_softmax_extraction(inp_file, out_file, checkpoints, bin_files):

  for i in xrange(0,len(bin_files)):
    print(("%s%d")%('File number ', i))
    with tf.Graph().as_default():
      # Get images and labels.
      images, labels = classification_functions.read_inputs(img_path=os.path.join( inp_file[0], bin_files[i]))
      logits, descriptors = classification_functions.inference(images)

      filename = (os.path.join( inp_file[0], bin_files[i]))
      filename = filename[0:len(filename)-1]
      ITERATIONS = os.path.getsize(filename)/(14888*classification_functions.batch_size) + 1

      variable_averages = tf.train.ExponentialMovingAverage(0.9999)
      variables_to_restore = {}
      for v in tf.all_variables():     
        if v in tf.trainable_variables():
          restore_name = variable_averages.average_name(v)
        else:
          restore_name = v.op.name
        variables_to_restore[restore_name] = v
      saver = tf.train.Saver(variables_to_restore)

      evaluate_and_extract(saver, logits, labels, descriptors, ITERATIONS, out_file, checkpoint_file = checkpoints, binfiles = bin_files[i])
    


def main(argv):  # pylint: disable=unused-argument
  
  inputfile = []
  outputfile = []
  checkpoint = []
  binfiles = []
  try:
    opts, args = getopt.getopt(argv,"hi:o:c:",["ifile=","ofile=", "cfile="])
  except getopt.GetoptError:
    print('test.py -i <inputfile> -o <outputpath> -c <checkpoint> ')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('test.py -i <inputfile> -o <outputfile>')
      print('outputfile is the name of the output file which will contains for each input patch the following data: ')
      print('')
      print('   -Softmax output for each patch, the size depends on the number of classess')
      print('   -2D position of the patch center ')
      print('   -Descriptor for each input patch ')
      print('')
      sys.exit()
    elif opt in ("-i", "--ifile"):
      inputfile.append(arg)
    elif opt in ("-o", "--ofile"):
      outputfile.append(arg)
    elif opt in ("-c", "--ifile"):
      checkpoint.append(arg)  

  
  with open(os.path.join(inputfile[0],'list.txt'), 'r') as openobjectfile:
    for line in openobjectfile:
      binfiles.append(line)

  if(len(inputfile) != 1 or len(outputfile) != 1 or len(checkpoint)!=1):
    print('')
    print('')
    print('usage: ')
    print('test.py -i <inputfile> -o <outputpath> -c <checkpoint>') 
    print('')
    print('-----------')
    print('')
    print('inputfile is the name of .bin files used as inputs for the features extraction and evaluation phase ')
    print('outputfile is the path for the output folder which will contains, for each input image, the following data: ')
    print('')
    print('   -Softmax output for each patch, the size depends on the number of classess')
    print('   -Descriptor for each input patch ')
    print('')
    print('checkpoint is the name of the storage file (for the network trained previously) used for the evaluation')
    print('')
    print('')
    sys.exit() 
    
  if os.path.exists( os.path.join(outputfile[0],'features') )==0:
    gfile.MakeDirs(os.path.join(outputfile[0],'features'))

  if os.path.exists(os.path.join(outputfile[0],'softmax'))==0:
    gfile.MakeDirs(os.path.join(outputfile[0],'softmax'))

  features_softmax_extraction(inp_file=inputfile, out_file=outputfile, checkpoints = checkpoint, bin_files=binfiles) 



if __name__ == '__main__':
  if os.path.exists(classification_functions.checkpoint_dir)==0:
    gfile.MakeDirs(classification_functions.checkpoint_dir)
  main(sys.argv[1:])

